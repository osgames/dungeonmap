////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2009 Anders Reggestad
// 
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#ifndef TILEITEM_H_
#define TILEITEM_H_

#include <QGraphicsItem>
#include <QImage>

#include "dungeonmaptile.h"

class TileItem : public QObject, public QGraphicsItem 
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)
        
 public:
    TileItem( const DungeonMapTile* a_tile, int a_posX, int a_posY, int a_tileSize, int a_rotate = 0);
    virtual ~TileItem();
    
    virtual QRectF boundingRect() const;

    virtual void paint(QPainter* a_painter, const QStyleOptionGraphicsItem* a_option,
                       QWidget* a_widget);

    bool isGroundTile() const;
    bool isObjectTile() const;
    bool isNoteTile() const;
    bool isFogOfWarTile() const;

    const DungeonMapTile* tile() const;
    int posX() const;
    int posY() const;
    int tileSize() const;
    int rotate() const;
    void setRotate( int a_rotate );

 protected:
    virtual void contextMenuEvent( QGraphicsSceneContextMenuEvent* a_event );

 private:

    const DungeonMapTile* m_tile;
    int m_posX;
    int m_posY;
    int m_tileSize;
    int m_rotate;
    
};

#endif
