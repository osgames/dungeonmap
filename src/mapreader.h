////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2009 Anders Reggestad
// 
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#ifndef MAPREADER_H_
#define MAPREADER_H_

#include <QXmlStreamReader>

#include "dungeonmaptile.h"
#include "dungeonmapsetup.h"
#include "dungeonmapscene.h"
#include "tileitem.h"

class MapReader : public QXmlStreamReader
{
 public:
    MapReader( DungeonMapSetup* a_setup, DungeonMapScene* a_scene );

    bool read( QIODevice* a_device );

 private:
    void readDMap();
    void readTiles();
    void readTile();
    void readLevel();
    void readMapTiles();
    void readMapTile();
    void readUnknownElement();
    
    DungeonMapSetup* m_setup;
    DungeonMapScene* m_scene;

    QMap< int, DungeonMapTile* > m_tiles;
    
};

#endif
