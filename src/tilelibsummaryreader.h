////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2013 Anders Reggestad
//
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#ifndef TILELIBSUMMARYREADER_H
#define TILELIBSUMMARYREADER_H

#include <QXmlStreamReader>
#include <QIODevice>
#include <QList>
#include <QDateTime>
#include <QUuid>

class TileLibSummaryReader : public QXmlStreamReader
{
public:
    class TileLibSummary
    {
    public:
        TileLibSummary(const QString& a_domain, const QString& a_name,
                       const int a_id, const QUuid& a_uuid, const QDateTime& a_created,
                       const QString& a_author, const QDateTime& a_modified,
                       const QString& a_modifier, int a_tileSize,
                       int a_size);

        int id() const;
        const QUuid& uuid() const;
        const QString& domain() const;
        const QString& name() const;
        const QDateTime& created() const;
        const QString& author() const;
        const QDateTime& modified() const;
        const QString& modifier() const;
        int tileSize() const;
        int size() const;

    private:
        int       m_id;
        QUuid     m_uuid;
        QString   m_domain;
        QString   m_name;
        QDateTime m_created;
        QString   m_author;
        QDateTime m_modified;
        QString   m_modifier;
        int       m_tileSize;
        int       m_size;
    };

    TileLibSummaryReader();
    ~TileLibSummaryReader();

    bool read( QIODevice* a_device );

    QList<TileLibSummary*>& list();

private:
    void readTileLibsSummaries();
    void readTileLibSummary();
    void readUnknownElement();

    double                 m_version;
    QList<TileLibSummary*> m_summaries;
};

#endif // TILELIBSUMMARYREADER_H
