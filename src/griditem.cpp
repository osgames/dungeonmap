////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2009 Anders Reggestad
// 
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#include <QPainter>
#include <QDebug>

#include "griditem.h"
#include "dungeonmapsetup.h"

GridItem::GridItem( DungeonMapSetup* a_setup )
{
    m_setup = a_setup;
    
    connect( m_setup, SIGNAL( prepareSizeChange() ), this, SLOT( prepareSizeChange() ));

    setZValue( DungeonMapSetup::GRID_ZVALUE );
}


GridItem::~GridItem()
{
    
}

void GridItem::prepareSizeChange()
{
    prepareGeometryChange ();
}


    
QRectF GridItem::boundingRect() const
{
    qreal penWidth = 1;
    return QRectF( -penWidth/2, -penWidth/2,
                   m_setup->width()*m_setup->tileSize()+penWidth,
                   m_setup->height()*m_setup->tileSize()+penWidth );
}

void GridItem::paint(QPainter* a_painter, const QStyleOptionGraphicsItem* a_option,
                     QWidget* a_widget)
{
    Q_UNUSED( a_option );
    Q_UNUSED( a_widget );

    int t = m_setup->tileSize();
    int w = m_setup->width()*t;
    int h = m_setup->height()*t;

    for (int i = 0; i <= m_setup->width(); i++)
    {
        a_painter->drawLine( i*t, 0, i*t, h );
    }
    for (int i = 0; i <= m_setup->height(); i++)
    {
        a_painter->drawLine( 0, i*t, w, i*t );
    }
}
