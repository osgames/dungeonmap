////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2009 Anders Reggestad
// 
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#include "mapwidget.h"

#include <QDebug>
#include <QWheelEvent>

#include <math.h>

#include "griditem.h"
#include "tileitem.h"
#include "compassitem.h"
#include "dungeonmapscene.h"
#include "dungeonmapsetup.h"


MapWidget::MapWidget( DungeonMapSetup* setup )
{
    m_scene = new DungeonMapScene(this, setup );
    setScene( m_scene );
    setRenderHint(QPainter::Antialiasing);
    setTransformationAnchor(AnchorUnderMouse);
    setResizeAnchor(AnchorUnderMouse);

    m_gridItem = new GridItem( setup );
    m_scene->addItem( m_gridItem );
    m_compassItem = new CompassItem( setup, "../images/compass.png" );
    m_scene->addItem( m_compassItem );

    setWindowTitle(tr("Dungeon Map"));

    m_modified = false;
    m_empty = true;
}

MapWidget::~MapWidget()
{
    
}

void MapWidget::itemMoved()
{
}

bool MapWidget::isModified()
{
    return m_modified;
}

void MapWidget::setModified( bool modified )
{
    m_modified = modified;
}

bool MapWidget::isEmpty()
{
    return m_empty;
}

DungeonMapScene* MapWidget::scene()
{
    return m_scene;
}

void MapWidget::toggleCompass(bool checked)
{
    m_compassItem->setVisible(checked);
}

void MapWidget::toggleNotes(bool checked)
{
    m_scene->toggleNotes( checked );
}

void MapWidget::keyPressEvent(QKeyEvent *event)
{
     switch (event->key()) {
     case Qt::Key_Up:
         //         centerNode->moveBy(0, -20);
         break;
     case Qt::Key_Down:
         //         centerNode->moveBy(0, 20);
         break;
     case Qt::Key_Left:
         //         centerNode->moveBy(-20, 0);
         break;
     case Qt::Key_Right:
         //         centerNode->moveBy(20, 0);
         break;
     case Qt::Key_Plus:
         scaleView(qreal(1.2));
         break;
     case Qt::Key_Minus:
         scaleView(1 / qreal(1.2));
         break;
     case Qt::Key_Space:
     case Qt::Key_Enter:
         /*
         foreach (QGraphicsItem *item, scene()->items()) {
             if (qgraphicsitem_cast<Node *>(item))
                 item->setPos(-150 + qrand() % 300, -150 + qrand() % 300);
         }
         */
         break;
     default:
         QGraphicsView::keyPressEvent(event);
     }
 }

 void MapWidget::wheelEvent(QWheelEvent *event)
 {
     scaleView(pow((double)2, event->delta() / 240.0));
 }

 void MapWidget::drawBackground(QPainter *painter, const QRectF &rect)
 {
     Q_UNUSED(rect);
     Q_UNUSED(painter);
 }

 void MapWidget::scaleView(qreal scaleFactor)
 {
     qreal factor = matrix().scale(scaleFactor, scaleFactor).mapRect(QRectF(0, 0, 1, 1)).width();
     if (factor < 0.07 || factor > 100)
         return;

     scale(scaleFactor, scaleFactor);
 }
