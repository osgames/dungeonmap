////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2009 Anders Reggestad
// 
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#ifndef GRIDITEM_H_
#define GRIDITEM_H_

#include <QObject>
#include <QGraphicsItem>

class DungeonMapSetup;

class GridItem : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)
        
 public:
    GridItem( DungeonMapSetup* a_setup );
    virtual ~GridItem();
    
    virtual QRectF boundingRect() const;

    virtual void paint( QPainter* a_painter, const QStyleOptionGraphicsItem* a_option,
                        QWidget* a_widget );

 private slots:
    virtual void prepareSizeChange();
    
 private:
    DungeonMapSetup* m_setup;
};

#endif
