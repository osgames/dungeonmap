////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2013 Anders Reggestad
//
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#ifndef TILELIB_H
#define TILELIB_H

#include <QString>
#include <QMap>
#include <QListWidget>
#include <QDateTime>

#include "dungeonmaptile.h"

class TileLibCollection;

class TileLib : public DungeonMapTileFactoryIf
{
public:
    TileLib(TileLibCollection* a_tileLibCollection);
    TileLib(TileLibCollection* a_tileLibCollection, const QString& a_domain,
            const QString& a_name);
    virtual ~TileLib();

    const QUuid& uuid() const;
    const QString& domain() const;
    const QString& name() const;
    const QDateTime& created() const;
    const QString& author() const;
    const QDateTime& modified() const;
    const QString& modifier() const;
    int tileSize() const;
    bool includesGroundTiles() const;
    bool includesObjectTiles() const;
    bool includesNoteTiles() const;
    bool includesFogOfWarTiles() const;
    bool includesTileType( DungeonMapTile::TileType a_tileType ) const;
    TileLibCollection* collection();
    QListWidget* listWidget(DungeonMapTile::TileType a_tileType);

    QMap<int, DungeonMapTile*>& tilesById();
    virtual DungeonMapTile* getTile( int a_id );
    virtual DungeonMapTile* getTile( const QString& a_uniqueId );
    virtual DungeonMapTile* getTile( const QUuid& a_uuid );

    void setUuid(const QUuid& a_uuid);
    void setDomain(const QString& domain);
    void setName(const QString& name);
    void setCreated(const QDateTime& created);
    void setAuthor(const QString& author);
    void setModified(const QDateTime& modified);
    void setModifier(const QString& modifier);
    void setTileSize(int a_tileSize);
    void setLibsToInclude(bool groundTiles, bool objectTiles, bool noteTiles, bool fogOfWarTiles);
    void setListWidget(DungeonMapTile::TileType a_tileType, QListWidget* a_widget);

    void addTile(DungeonMapTile* a_tile, bool updateDirtyFlag = false);

    bool isDirty() const;
    void setDirty(bool dirty);

    bool isShared() const;
    void setShared(bool shared);

private:
    QUuid                           m_uuid;
    QString                         m_domain;
    QString                         m_name;
    QDateTime                       m_created;
    QString                         m_author;
    QDateTime                       m_modified;
    QString                         m_modifier;
    int                             m_tileSize;
    bool                            m_includesGroundTiles;
    bool                            m_includesObjectTiles;
    bool                            m_includesNoteTiles;
    bool                            m_includesFogOfWarTiles;
    TileLibCollection*              m_tileLibCollection;

    int                             m_nextId;
    bool                            m_dirty;
    bool                            m_shared;

    QMap<int,     DungeonMapTile*>  m_tilesById;
    QMap<QUuid,   DungeonMapTile*>  m_tilesByUuid;
    QMap<QString, DungeonMapTile*>  m_tilesByUniqueId;

    QMap<DungeonMapTile::TileType, QListWidget*> m_listWidgets;
};

#endif // TILELIB_H
