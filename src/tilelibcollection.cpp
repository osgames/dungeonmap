////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2013 Anders Reggestad
//
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#include "tilelibcollection.h"

TileLibCollection::TileLibCollection() :
    m_nextTileId(1)
{
}

TileLibCollection::~TileLibCollection()
{
    qDeleteAll( m_tileLibs );
}

int TileLibCollection::getNextTileId()
{
    return m_nextTileId++;
}

void TileLibCollection::addTileLib(TileLib* a_tileLib)
{
    if (!this->getTileLib(a_tileLib->uuid()))
    {
        m_tileLibs.append(a_tileLib);
        m_tileLibsByUuid[a_tileLib->uuid()] = a_tileLib;
        m_tileLibsByName[a_tileLib->name()] = a_tileLib;
    }
}

void TileLibCollection::addTile(DungeonMapTile* a_tile)
{
    m_tilesById[a_tile->id()] = a_tile;
    m_tilesByUuid[a_tile->uuid()] = a_tile;
    m_tilesByUniqueId[a_tile->uniqueId()] = a_tile;
}

DungeonMapTile* TileLibCollection::getTile( int a_id )
{
    return m_tilesById[ a_id ];
}

DungeonMapTile* TileLibCollection::getTile( const QString& a_uniqueId )
{
    return m_tilesByUniqueId[ a_uniqueId ];
}

DungeonMapTile* TileLibCollection::getTile( const QUuid& a_uuid )
{
    return m_tilesByUuid[ a_uuid ];
}

QList<TileLib*>& TileLibCollection::map()
{
    return m_tileLibs;
}

TileLib* TileLibCollection::getTileLib( const QUuid& a_uuid )
{
    return m_tileLibsByUuid[ a_uuid ];
}

TileLib* TileLibCollection::getTileLib( const QString& a_name )
{
    return m_tileLibsByName[ a_name ];
}

void TileLibCollection::setDockWidgets(DungeonMapTile::TileType a_tileType, QStackedWidget* a_stackedWidget, QComboBox* a_pageWidget)
{
    m_stackedWidget[a_tileType] = a_stackedWidget;
    m_pageWidget[a_tileType] = a_pageWidget;
}

QStackedWidget* TileLibCollection::getStackedWidget(DungeonMapTile::TileType a_tileType)
{
    return m_stackedWidget[a_tileType];
}

QComboBox* TileLibCollection::getPageWidget(DungeonMapTile::TileType a_tileType)
{
    return m_pageWidget[a_tileType];
}
