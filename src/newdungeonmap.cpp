////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2009 Anders Reggestad
// 
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#include "newdungeonmap.h"
#include "ui_newdungeonmap.h"

NewDungeonMap::NewDungeonMap( QWidget* parent )
    : QDialog( parent ),
    m_ui(new Ui::NewDungeonMap)
{
    m_ui->setupUi( this );

    m_ui->tileSize->addItem("72 dpi",QVariant(DungeonMapSetup::TILE_SIZE_72_DPI));
    m_ui->tileSize->addItem("96 dpi",QVariant(DungeonMapSetup::TILE_SIZE_96_DPI));
    m_ui->tileSize->addItem("100 dpi",QVariant(DungeonMapSetup::TILE_SIZE_100_DPI));
    m_ui->tileSize->addItem("150 dpi",QVariant(DungeonMapSetup::TILE_SIZE_150_DPI));
    m_ui->tileSize->addItem("300 dpi",QVariant(DungeonMapSetup::TILE_SIZE_300_DPI));
    m_ui->tileSize->addItem("600 dpi",QVariant(DungeonMapSetup::TILE_SIZE_600_DPI));
    m_ui->tileSize->addItem("1200 dpi",QVariant(DungeonMapSetup::TILE_SIZE_1200_DPI));
}


DungeonMapSetup* NewDungeonMap::getSetup( QWidget* parent )
{
    NewDungeonMap dlg( parent );
    DungeonMapSetup* setup = new DungeonMapSetup;

    dlg.setWidth( setup->width() );
    dlg.setHeight( setup->height() );
    
    if ( dlg.exec() == QDialog::Accepted )
    {
        setup->setWidth( dlg.width() );
        setup->setHeight( dlg.height() );
        setup->setTileSize( dlg.tileSize() );
        return setup;
    }

    delete setup;
    return NULL;
}

void NewDungeonMap::setHeight( int a_height )
{
    m_ui->mapHeight->setValue( a_height );
}

void NewDungeonMap::setWidth( int a_width )
{
    m_ui->mapWidth->setValue( a_width );
}

int NewDungeonMap::height()
{
    return m_ui->mapHeight->value();
}

int NewDungeonMap::width()
{
    return m_ui->mapWidth->value();
}

DungeonMapSetup::TileSize NewDungeonMap::tileSize()
{
    return (DungeonMapSetup::TileSize)m_ui->tileSize->itemData(m_ui->tileSize->currentIndex()).toInt();
}
