////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2013 Anders Reggestad
//
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#ifndef NEWTILELIB_H
#define NEWTILELIB_H

#include <QDialog>
#include <QStatusBar>

#include "tilelibcollection.h"

namespace Ui {
class NewTileLib;
}

class NewTileLib : public QDialog
{
    Q_OBJECT
    
public:
    explicit NewTileLib(TileLibCollection* a_tileLibCollection, QStatusBar* a_statusBar, QWidget *parent = 0);
    ~NewTileLib();
    void setLibsToInclude(bool groundTiles, bool objectTiles, bool noteTiles, bool fogOfWarTiles);
    void setAuthor(const QString& author);

    const QString libName() const;
    const QString libDomain() const;
    const QString author() const;
    bool includesGroundTiles() const;
    bool includesObjectTiles() const;
    bool includesNoteTiles() const;
    bool includesFogOfWarTiles() const;
    int tileSize() const;
    
private:
    virtual void done(int r);

    Ui::NewTileLib *ui;
    TileLibCollection *m_tileLibCollection;
    QStatusBar* m_statusBar;
};

#endif // NEWTILELIB_H
