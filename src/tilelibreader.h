////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2013 Anders Reggestad
//
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#ifndef TILELIBREADER_H
#define TILELIBREADER_H

#include <QXmlStreamReader>
#include <QString>

#include "tilelib.h"

class TileLibReader : public QXmlStreamReader
{
public:
    TileLibReader(TileLib* a_tileLib);

    bool read( QIODevice* a_device );

private:
    bool boolAttribute(const QString& a_name, bool a_default);
    void readTileLib();
    void readTiles();
    void readTile();
    QImage readImage();
    void readUnknownElement();


    TileLib* m_tileLib;
    double   m_version;
};

#endif // TILELIBREADER_H
