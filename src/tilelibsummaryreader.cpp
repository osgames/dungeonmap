////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2013 Anders Reggestad
//
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#include "tilelibsummaryreader.h"


TileLibSummaryReader::TileLibSummary::TileLibSummary(const QString& a_domain, const QString& a_name,
                                                     const int a_id, const QUuid& a_uuid, const QDateTime& a_created,
                                                     const QString& a_author, const QDateTime& a_modified,
                                                     const QString& a_modifier, int a_tileSize,
                                                     int a_size):
    m_id(a_id),
    m_uuid(a_uuid),
    m_domain(a_domain),
    m_name(a_name),
    m_created(a_created),
    m_author(a_author),
    m_modified(a_modified),
    m_modifier(a_modifier),
    m_tileSize(a_tileSize),
    m_size(a_size)
{
}

int TileLibSummaryReader::TileLibSummary::id()const
{
    return m_id;
}
const QUuid& TileLibSummaryReader::TileLibSummary::uuid() const
{
    return m_uuid;
}

const QString& TileLibSummaryReader::TileLibSummary::domain() const
{
    return m_domain;
}

const QString& TileLibSummaryReader::TileLibSummary::name() const
{
    return m_name;
}

const QDateTime& TileLibSummaryReader::TileLibSummary::created() const
{
    return m_created;
}

const QString& TileLibSummaryReader::TileLibSummary::author() const
{
    return m_author;
}

const QDateTime& TileLibSummaryReader::TileLibSummary::modified() const
{
    return m_modified;
}

const QString& TileLibSummaryReader::TileLibSummary::modifier() const
{
    return m_modifier;
}

int TileLibSummaryReader::TileLibSummary::tileSize() const
{
    return m_tileSize;
}

int TileLibSummaryReader::TileLibSummary::size() const
{
    return m_size;
}


TileLibSummaryReader::TileLibSummaryReader()
{
}

TileLibSummaryReader::~TileLibSummaryReader()
{
    qDeleteAll(m_summaries);
}


QList<TileLibSummaryReader::TileLibSummary*>& TileLibSummaryReader::list()
{
    return m_summaries;
}

bool TileLibSummaryReader::read(QIODevice *a_device)
{
    setDevice(a_device);

    while (!atEnd())
    {
        readNext();

        if (isStartElement())
        {
            if (name() == "tilelib_summaries")
            {
                m_version = attributes().value("version").toString().toDouble();

                if (m_version == 1.0)
                {
                   readTileLibsSummaries();
                }
                else
                {
                    raiseError(QObject::tr("This Dungeon Mapper Tile Lib is of unkonwn version."));
                }
            }
            else
            {
                raiseError(QObject::tr("The file is not an Dungeon Mapper Tile Lib file."));
            }
        }
    }

    return !error();

}

void TileLibSummaryReader::readTileLibsSummaries()
{
    Q_ASSERT(isStartElement() && name() == "tilelib_summaries" );

    while ( !atEnd() )
    {
        readNext();

        if ( isEndElement() )
            break;

         if ( isStartElement() )
        {
            if (name() == "summary")
            {
                readTileLibSummary();
            }
            else
            {
                readUnknownElement();
            }
         }
     }
}

void TileLibSummaryReader::readTileLibSummary()
{
    Q_ASSERT( isStartElement() && name() == "summary" );

    QString domain = attributes().value("domain").toString();
    QString name = attributes().value("name").toString();
    int id = attributes().value("id").toString().toInt();
    QUuid uuid = QUuid(attributes().value("uuid").toString());
    QDateTime created = QDateTime::fromString(attributes().value(("created")).toString(),Qt::ISODate);
    QString author = attributes().value("author").toString();
    QDateTime modified = QDateTime::fromString(attributes().value(("modified")).toString(),Qt::ISODate);
    QString modifier = attributes().value("modifier").toString();
    int tileSize = attributes().value("tile_size").toString().toInt();
    int size = attributes().value("size").toString().toInt();

    m_summaries.append(new TileLibSummary(domain,name,id,uuid,created,author,modified,modifier,tileSize,size));

    // Read anything until next end element
    readUnknownElement();
}

void TileLibSummaryReader::readUnknownElement()
{
    Q_ASSERT( isStartElement() );

    while ( !atEnd() )
    {
        readNext();

        if ( isEndElement() )
            break;

        if ( isStartElement() )
        {
            readUnknownElement();
        }
     }
 }
