////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2009 Anders Reggestad
// 
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QMainWindow>
#include <QListWidget>
#include <QMap>
#include <QPrinter>
#include <QAction>
#include <QActionGroup>
#include <QMenu>
#include <QTextEdit>
#include <QUndoGroup>
#include <QComboBox>
#include <QNetworkAccessManager>

#include "mapwidget.h"
#include "dungeonmapsetup.h"
#include "dungeonmaptile.h"
#include "tilelib.h"


class MainWindow : public QMainWindow //, DungeonMapTileFactoryIf
{
     Q_OBJECT

    static const int MAJOR_VERSION = 1;
    static const int MINOR_VERSION = 3;
    static const int PATCH_LEVEL = 0;
    static const char* NEW_LIB;

 public:
     MainWindow();
     MainWindow( const QString& a_fileName );
     virtual ~MainWindow();

     bool init( DungeonMapSetup* a_setup );

     bool saveLib(TileLib* a_tileLib);
     void addLib(TileLib* a_tileLib);

 protected:
     void closeEvent( QCloseEvent* a_event );

 private slots:
     void newFile();
     void open();
     bool save();
     bool saveAs();
     void openRecentFile();
     void print();
     void printPreview(QPrinter *printer);
     void exportMap();
     void filePrintPdf();

     void manageLibs();
     bool saveLibs();
     void newLib();
     void newTile();

     void about();

     void documentWasModified();
     void tileSelectionChanged ( QListWidgetItem* a_current, QListWidgetItem* a_previous );
     void paintToolSelected();
     void fillToolSelected();


signals:
     void clearTileLibSelection();

private:

     bool loadLibs();
     bool loadLib(const QString& a_fileName);

     QString libPath() const;
     bool saveLib(const QString& path, TileLib* a_tileLib);

     void createActions();
     void createMenus();
     void createToolBars();
     void createStatusBar();
     void createDockWindows();
     void createTileLibDockWindow(const QString & a_title, DungeonMapTile::TileType a_tileType);
     void createTileLibDockWindow(TileLib* a_tileLib, DungeonMapTile::TileType a_tileType);
     void createTileLibDockWindow(TileLib* a_tileLib, DungeonMapTile::TileType a_tileType, DungeonMapTile* a_tile);

     QString userName() const;
     void readSettings();
     void writeSettings();
     bool maybeSave();
     void openFile(const QString& a_fileName);
     void loadFile(const QString& a_fileName);
     bool saveFile(const QString& a_fileName);
     void setCurrentFile(const QString& a_fileName);
     void updateRecentFileActions();
     QString strippedName(const QString& a_fullFileName);
     MainWindow *findMainWindow(const QString& a_fileName);

     QNetworkAccessManager m_networkAccessManager;

     DungeonMapSetup* m_setup;
     
     MapWidget *m_mapWidget;
     QString curFile;
     bool isUntitled;

     QMenu *fileMenu;
     QMenu *editMenu;
     QMenu *viewMenu;
     QMenu *libMenu;
     QMenu *helpMenu;
     QToolBar *fileToolBar;
     QToolBar *editToolBar;
     QToolBar *viewToolBar;

     // File actions
     QAction *newAct;
     QAction *openAct;
     QAction *saveAct;
     QAction *saveAsAct;
     QAction *separatorAct;
     enum { MaxRecentFiles = 5 };
     QAction* recentFileActs[MaxRecentFiles];
     QAction *printAct;
     QAction *exportAct;
     QAction *exportPdfAct;
     QAction *closeAct;
     QAction *exitAct;

     // Edit actions
     QUndoGroup *m_undoGroup;
     QAction *undoAct;
     QAction *redoAct;
     QAction *paintAct;
     QAction *fillAct;
     QActionGroup* toolActGroup;
     QAction *cutAct;
     QAction *copyAct;
     QAction *pasteAct;

     // View actions
     QAction *noteAct;
     QAction *compassAct;

     // Lib actions
     QAction *manageLibsAct;
     QAction *saveLibsAct;
     QAction *newLibAct;
     QAction *newTileAct;

     // Help actions
     QAction *aboutAct;

     TileLibCollection* m_tileLibCollection;
     QString m_author; // Name of the current author

/*
     QListWidget* groundTiles;
     QComboBox*   groundTileLibSelect;
     QListWidget* objectTiles;
     QComboBox*   objectTileLibSelect;
*/

     int                          m_tileRole;

     
 };

 #endif
