////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2013 Anders Reggestad
//
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#ifndef COMMANDS_H
#define COMMANDS_H

#include <QUndoCommand>
#include "dungeonmapscene.h"
#include "dungeonmaptile.h"
#include "tileitem.h"

class AddGroundTileCommand : public QUndoCommand
{
public:
    AddGroundTileCommand(DungeonMapScene *a_scene, TileItem* a_prevTileItem, const DungeonMapTile* a_tile, int a_posX, int a_posY, int a_tileSize, QUndoCommand *parent = 0);
    void undo();
    void redo();
private:
    DungeonMapScene* m_scene;
    TileItem*        m_prevTileItem;
    TileItem*        m_newTileItem;
};

class AddItemTileCommand : public QUndoCommand
{
public:
    AddItemTileCommand(DungeonMapScene *a_scene, const DungeonMapTile* a_tile, int a_posX, int a_posY, int a_tileSize, QUndoCommand *parent = 0);
    void undo();
    void redo();
private:
    DungeonMapScene* m_scene;
    TileItem*        m_newTileItem;
};

class RemoveItemTileCommand : public QUndoCommand
{
public:
    RemoveItemTileCommand(DungeonMapScene *a_scene, TileItem* a_item, QUndoCommand *parent = 0);
    void undo();
    void redo();
private:
    DungeonMapScene* m_scene;
    TileItem*        m_item;
};

class RotateItemTileCommand : public QUndoCommand
{
public:
    RotateItemTileCommand(DungeonMapScene *a_scene, TileItem* a_item, int a_rotate, QUndoCommand *parent = 0);
    void undo();
    void redo();
private:
    DungeonMapScene* m_scene;
    TileItem*        m_item;
    int              m_newRotate;
    int              m_prevRotate;
};

#endif // COMMANDS_H
