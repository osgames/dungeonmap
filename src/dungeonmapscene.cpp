////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2009 Anders Reggestad
// 
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#include <QGraphicsSceneMouseEvent>
#include <QWidget>
#include <QMessageBox>

#include "dungeonmapscene.h"
#include "tileitem.h"
#include "commands.h"
#include "tilelib.h"

DungeonMapScene::DungeonMapScene( QWidget* parent, DungeonMapSetup* setup ):
    m_viewNotes( true )
{
    m_parent = parent;
    
    m_setup = setup;

    m_undoStack = new QUndoStack(this);

    m_isPainting = false;
}

DungeonMapScene::~DungeonMapScene()
{
    delete m_undoStack;
}

void DungeonMapScene::mousePressEvent ( QGraphicsSceneMouseEvent * mouseEvent )
{
    if ( mouseEvent->button() != Qt::LeftButton )
    {
        mouseEvent->ignore();
        return;
    }
    
    m_isPainting = true;

    QPointF scenePos = mouseEvent->scenePos();
        
    QPoint point( (int)(scenePos.x() / m_setup->tileSize()), (int)(scenePos.y() / m_setup->tileSize()) );
    
    if ( point.x() >= 0 && point.x() < m_setup->width() && 
         point.y() >= 0 && point.y() < m_setup->height() )
    {
        handleMouseDownAt( point );
    }
    m_lastPoint = point;
}

void DungeonMapScene::mouseMoveEvent ( QGraphicsSceneMouseEvent * mouseEvent )
{
    if ( !m_isPainting ) return;

    QPointF scenePos = mouseEvent->scenePos();
    
    QPoint point( (int)(scenePos.x() / m_setup->tileSize()), (int)(scenePos.y() / m_setup->tileSize()) );
    
    if ( point.x() >= 0 && point.x() < m_setup->width() && 
         point.y() >= 0 && point.y() < m_setup->height() &&
         point != m_lastPoint )
    {
        handleMouseDownAt( point );
    }
    m_lastPoint = point;
}


void DungeonMapScene::mouseReleaseEvent ( QGraphicsSceneMouseEvent * mouseEvent )
{
    if ( mouseEvent->button() != Qt::LeftButton )
    {
        mouseEvent->ignore();
        return;
    }
    
    
    m_isPainting = false;
}

void DungeonMapScene::handleMouseDownAt( const QPoint& point )
{
    int lastId = -1;
    
    if ( m_setup->currentTile() )
    {
        if (m_setup->checkTileSizeAtPaint() && m_setup->currentTile()->tileLib()->tileSize() != m_setup->tileSize())
        {
            m_isPainting = false; // Dialog is interrupting the paint job

            int ret = QMessageBox::warning(m_parent,tr("Different Resolution"),tr("Tile has a different reselution than the map\n")+
                                           tr("Do you want to continue?\n")+tr("This warning will be turned off if you continue."),
                                           QMessageBox::Yes | QMessageBox::No, QMessageBox::No);
            if (ret == QMessageBox::No) return;

            m_setup->setCheckTileSizeAtPaint(false);
        }

        // Replace any existing ground tiles
        if ( m_setup->currentTile()->isGroundTile() )
        {
            TileItem* tileItem = findGroundTileAt( point );
            
            if ( tileItem )
            {
                lastId = tileItem->tile()->id();

                // No point in doing anything if the same type of tile is there allready
                if ( lastId == m_setup->currentTile()->id() ) return;
            
                //removeItem( tileItem );
            }

            if ( m_setup->currentTool() == DTT_FILL )
            {
                m_parent->setCursor( Qt::WaitCursor );

                undoStack()->beginMacro("Fill");
                doFill( point, lastId);
                doFill( point + QPoint( 1, 0), lastId );
                doFill( point + QPoint(-1, 0), lastId );
                doFill( point + QPoint( 0, 1), lastId );
                doFill( point + QPoint( 0,-1), lastId );
                undoStack()->endMacro();

                m_parent->unsetCursor();
            }
            else
            {
                undoStack()->push(new AddGroundTileCommand(this, tileItem, m_setup->currentTile(), point.x(), point.y(), m_setup->tileSize()));
            }
        }
        else if (m_setup->currentTile()->isObjectTile() || m_setup->currentTile()->isNoteTile())
        {
            undoStack()->push( new AddItemTileCommand(this, m_setup->currentTile(), point.x(), point.y(), m_setup->tileSize()));
        }

        contentsChanged();
    }
}

QUndoStack* DungeonMapScene::undoStack() const
{
    return m_undoStack;
}

void DungeonMapScene::toggleNotes(bool checked)
{
    m_viewNotes = checked;

    foreach (TileItem* item, m_notes)
    {
        item->setEnabled(m_viewNotes);
    }
}

bool DungeonMapScene::viewNotes() const
{
    return m_viewNotes;
}

void DungeonMapScene::addTileItem(TileItem *item)
{
    addItem(item);

    if (item->isNoteTile())
    {
        item->setEnabled(m_viewNotes);
        m_notes.append(item);
    }
}

void DungeonMapScene::removeTileItem(TileItem *item)
{
    removeItem(item);
    if (item->isNoteTile())
    {
        m_notes.removeOne(item);
    }
}

void DungeonMapScene::doFill( const QPoint& point, int lastId )
{
    if ( point.x() >= 0 && point.x() < m_setup->width() &&
         point.y() >= 0 && point.y() < m_setup->height() )
    {
        TileItem* tileItem = findGroundTileAt( point );

        if ( tileItem == NULL && lastId != -1 ) return;

        if ( ( tileItem == NULL && lastId == -1 ) ||
             ( tileItem->tile()->id() == lastId ) )
        {

            undoStack()->push(new AddGroundTileCommand(this, tileItem, m_setup->currentTile(), point.x(), point.y(), m_setup->tileSize()));

            // Continue to recurse
            doFill( point + QPoint(1,0) , lastId );
            doFill( point + QPoint(-1,0) , lastId );
            doFill( point + QPoint(0,1) , lastId );
            doFill( point + QPoint(0,-1) , lastId );
        }
    }
}


TileItem* DungeonMapScene::findGroundTileAt( const QPoint& point )
{
    QPointF scenPoint( point.x()*m_setup->tileSize(), point.y()*m_setup->tileSize() );

    QList<QGraphicsItem *> itemList = items( scenPoint );
            
    foreach (QGraphicsItem* item, itemList )
    {
        TileItem* tileItem = dynamic_cast<TileItem*>(item);
        if ( tileItem && tileItem->isGroundTile() )
        {
            return tileItem;
        }
    }
    return NULL;
}
