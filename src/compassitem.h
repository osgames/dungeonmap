////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2013 Anders Reggestad
//
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#ifndef COMPASSITEM_H
#define COMPASSITEM_H

#include <QObject>
#include <QGraphicsItem>
#include <QImage>

class DungeonMapSetup;

class CompassItem : public QObject, public QGraphicsItem
{
    Q_OBJECT
    Q_INTERFACES(QGraphicsItem)

 public:
    CompassItem( DungeonMapSetup* a_setup, const QString& a_imageName );
    virtual ~CompassItem();

    virtual QRectF boundingRect() const;

    virtual void paint( QPainter* a_painter, const QStyleOptionGraphicsItem* a_option,
                        QWidget* a_widget );

 private slots:
    virtual void prepareSizeChange();

 private:
    DungeonMapSetup* m_setup;
    QImage           m_image;
};

#endif // COMPASSITEM_H
