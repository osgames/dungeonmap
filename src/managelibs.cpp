////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2013 Anders Reggestad
//
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#include <QHttpMultiPart>
#include <QBuffer>
#include <QNetworkReply>
#include <QObjectUserData>
#include <QMessageBox>
#include <QDataStream>

#include "managelibs.h"
#include "ui_managelibs.h"
#include "tilelibreader.h"
#include "tilelibwriter.h"
#include "tilelibsummaryreader.h"
#include "mainwindow.h"

ManageLibs::ManageLibs(QNetworkAccessManager* a_networkAccessManager, TileLibCollection* a_tileLibCollection, QStatusBar* a_statusBar, DungeonMapSetup* a_setup, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ManageLibs),
    m_networkAccessManager(a_networkAccessManager),
    m_tileLibCollection(a_tileLibCollection),
    m_setup(a_setup),
    m_statusBar(a_statusBar),
    m_uploadTileLib(NULL),
    m_downloadReply(NULL),
    m_listReply(NULL),
    m_uploadReply(NULL)
{
    ui->setupUi(this);

    // For now handle this as an download list
    ui->m_newList->setColumnCount(8);
    ui->m_newList->headerItem()->setText(0,tr("Domain"));
    ui->m_newList->headerItem()->setText(1,tr("Name"));
    ui->m_newList->headerItem()->setText(2,tr("Resolution"));
    ui->m_newList->headerItem()->setText(3,tr("Created"));
    ui->m_newList->headerItem()->setText(4,tr("Author"));
    ui->m_newList->headerItem()->setText(5,tr("Modified"));
    ui->m_newList->headerItem()->setText(6,tr("Modifier"));
    ui->m_newList->headerItem()->setText(7,tr("Size"));

    // While new list is a download list, disable the updated list.
    ui->m_tabWidget->removeTab(1);

    ui->m_uploadList->setColumnCount(7);
    ui->m_uploadList->headerItem()->setText(0,tr("Domain"));
    ui->m_uploadList->headerItem()->setText(1,tr("Name"));
    ui->m_uploadList->headerItem()->setText(2,tr("Resolution"));
    ui->m_uploadList->headerItem()->setText(3,tr("Created"));
    ui->m_uploadList->headerItem()->setText(4,tr("Author"));
    ui->m_uploadList->headerItem()->setText(5,tr("Modified"));
    ui->m_uploadList->headerItem()->setText(6,tr("Modifier"));

    QList<QTreeWidgetItem *> items;
    foreach (TileLib* lib, m_tileLibCollection->map())
    {
        // Do not add the standard lib...
        if (lib->name() == tr("Standard")) continue;

        QTreeWidgetItem* item = new QTreeWidgetItem();
        item->setData(0, Qt::UserRole, QVariant(lib->uuid()));

        item->setData(0, Qt::DisplayRole, QVariant(lib->domain()));
        item->setData(1, Qt::DisplayRole, QVariant(lib->name()));
        item->setData(2, Qt::DisplayRole, QVariant(lib->tileSize()));
        item->setData(3, Qt::DisplayRole, QVariant(lib->created()));
        item->setData(4, Qt::DisplayRole, QVariant(lib->author()));
        if (!lib->modifier().isEmpty())
        {
           item->setData(5, Qt::DisplayRole, QVariant(lib->modified()));
           item->setData(6, Qt::DisplayRole, QVariant(lib->modifier()));
        }
        items.append(item);
    }
    ui->m_uploadList->insertTopLevelItems(0, items);
    for(int i = 0; i < 7; i++)
           ui->m_uploadList->resizeColumnToContents(i);


    connect(ui->m_download,SIGNAL(clicked()),this,SLOT(download()));
    connect(ui->m_newList, SIGNAL(currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)),
            this,SLOT(downloadItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)));
    connect(ui->m_update,SIGNAL(clicked()),this,SLOT(update()));
    connect(ui->m_updateList,SIGNAL(currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)),
            this,SLOT(updateItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)));
    connect(ui->m_upload,SIGNAL(clicked()),this,SLOT(upload()));
    connect(ui->m_uploadList,SIGNAL(currentItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)),
            this,SLOT(uploadItemChanged(QTreeWidgetItem*,QTreeWidgetItem*)));
    connect(ui->m_tabWidget,SIGNAL(currentChanged(int)),this,SLOT(tabChanged(int)));


    connect(m_networkAccessManager, SIGNAL(finished(QNetworkReply *)), this, SLOT(replyFinished(QNetworkReply *)));

    requestList();
}

ManageLibs::~ManageLibs()
{
    delete ui;
}

void ManageLibs::tabChanged(int index)
{
    if (ui->m_tabWidget->currentWidget() == ui->m_newTab)
    {
        requestList();
    }
}

void ManageLibs::download()
{
    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

    QHttpPart uuidPart;
    uuidPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"uuid\""));
    uuidPart.setBody(m_downloadLibUuid.toString().toLatin1());

    multiPart->append(uuidPart);

    QHttpPart idPart;
    idPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"id\""));
    idPart.setBody(QString::number(m_downloadLibId).toLatin1());

    multiPart->append(idPart);

    QUrl url(m_setup->serverUrl() + "/services/download_dungeon_mapper_lib.php");
    QNetworkRequest request(url);

    m_downloadReply = m_networkAccessManager->post(request, multiPart);
    multiPart->setParent(m_downloadReply); // delete the multiPart with the reply

    connect(m_downloadReply,SIGNAL(downloadProgress(qint64,qint64)),this,SLOT(updateDataReadProgress(qint64,qint64)));
}

void ManageLibs::downloadItemChanged(QTreeWidgetItem* current, QTreeWidgetItem* previous)
{
    Q_UNUSED(previous);

    if (current)
    {
       m_downloadLibId = current->data(0,Qt::UserRole).toInt();
       m_downloadLibUuid = current->data(1,Qt::UserRole).toUuid();
    }
    else
    {
        m_downloadLibId = -1;
        m_downloadLibUuid = QUuid();
    }
    ui->m_progressBar->setValue(0);
}


void ManageLibs::update()
{

}

void ManageLibs::updateItemChanged(QTreeWidgetItem* current, QTreeWidgetItem* previous)
{

}

void ManageLibs::upload()
{
    if (m_uploadTileLib)
    {
        upload(m_uploadTileLib);
    }
}


void ManageLibs::upload(TileLib* lib)
{
    QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

    QHttpPart uuidPart;
    uuidPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"uuid\""));
    uuidPart.setBody(lib->uuid().toString().toLatin1());

    QHttpPart domainPart;
    domainPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"domain\""));
    domainPart.setBody(lib->domain().toLatin1());

    QHttpPart namePart;
    namePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"name\""));
    namePart.setBody(lib->name().toLatin1());

    QHttpPart createdPart;
    createdPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"created\""));
    createdPart.setBody(lib->created().toString(Qt::ISODate).toLatin1());

    QHttpPart authorPart;
    authorPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"author\""));
    authorPart.setBody(lib->author().toLatin1());

    QHttpPart modifiedPart;
    modifiedPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"modified\""));
    modifiedPart.setBody(lib->modified().toString(Qt::ISODate).toLatin1());

    QHttpPart modifierPart;
    modifierPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"modifier\""));
    modifierPart.setBody(lib->modifier().toLatin1());

    QHttpPart tileSizePart;
    tileSizePart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"tile_size\""));
    tileSizePart.setBody(QString::number(lib->tileSize()).toLatin1());

    QHttpPart libPart;
    libPart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/xml"));
    libPart.setHeader(QNetworkRequest::ContentDispositionHeader, QVariant("form-data; name=\"file\"; filename=\""+lib->name()+"\""));
    QByteArray libData;
    QBuffer buffer(&libData);
    buffer.open(QIODevice::WriteOnly);

    TileLibWriter writer( lib );
    if ( !writer.writeFile( &buffer ) )
    {
        return;
    }
    libPart.setBody(libData);

    multiPart->append(uuidPart);
    multiPart->append(domainPart);
    multiPart->append(namePart);
    multiPart->append(tileSizePart);
    multiPart->append(createdPart);
    multiPart->append(authorPart);
    multiPart->append(modifiedPart);
    multiPart->append(modifierPart);
    multiPart->append(libPart);

    QUrl url(m_setup->serverUrl() + "/services/upload_dungeon_mapper_lib.php");
    QNetworkRequest request(url);

    m_uploadReply = m_networkAccessManager->post(request, multiPart);
    multiPart->setParent(m_uploadReply); // delete the multiPart with the reply

    connect(m_uploadReply,SIGNAL(uploadProgress(qint64,qint64)),this,SLOT(updateDataReadProgress(qint64,qint64)));
    connect(m_uploadReply,SIGNAL(downloadProgress(qint64,qint64)),this,SLOT(updateDataReadProgress(qint64,qint64)));
}

void ManageLibs::requestList()
{
    QUrl url(m_setup->serverUrl() + "/services/list_dungeon_mapper_libs.php");
    QNetworkRequest request(url);

    m_listReply = m_networkAccessManager->get(request);

    connect(m_listReply,SIGNAL(downloadProgress(qint64,qint64)),this,SLOT(updateDataReadProgress(qint64,qint64)));
}

void ManageLibs::uploadItemChanged(QTreeWidgetItem* current,QTreeWidgetItem* previous)
{
    Q_UNUSED(previous);
    if (current)
    {
       m_uploadTileLib = m_tileLibCollection->getTileLib(current->data(0,Qt::UserRole).toUuid());
    }
    else
    {
        m_uploadTileLib = NULL;
    }
    ui->m_progressBar->setValue(0);
}

void ManageLibs::updateDataReadProgress(qint64 bytesRead, qint64 totalBytes)
{
    ui->m_progressBar->setRange(0,totalBytes);
    ui->m_progressBar->setValue(bytesRead);
}

void ManageLibs::uploadReplyFinished(QNetworkReply * a_reply)
{
    // Cleare the progress bar
    ui->m_progressBar->setRange(0,1);
    ui->m_progressBar->setValue(0);
}

void ManageLibs::listReplyFinished(QNetworkReply * a_reply)
{
    TileLibSummaryReader reader;

    reader.read(a_reply);

    ui->m_newList->clear();

    QList<QTreeWidgetItem *> items;
    foreach (TileLibSummaryReader::TileLibSummary* summary, reader.list())
    {

        QTreeWidgetItem* item = new QTreeWidgetItem();
        item->setData(0, Qt::UserRole, QVariant(summary->id()));
        item->setData(1, Qt::UserRole, QVariant(summary->uuid()));

        item->setData(0, Qt::DisplayRole, QVariant(summary->domain()));
        item->setData(1, Qt::DisplayRole, QVariant(summary->name()));
        item->setData(2, Qt::DisplayRole, QVariant(summary->tileSize()));
        item->setData(3, Qt::DisplayRole, QVariant(summary->created()));
        item->setData(4, Qt::DisplayRole, QVariant(summary->author()));
        item->setData(5, Qt::DisplayRole, QVariant(summary->modified()));
        item->setData(6, Qt::DisplayRole, QVariant(summary->modifier()));
        item->setData(7, Qt::DisplayRole, QVariant(summary->size()));
        items.append(item);
    }
    ui->m_newList->insertTopLevelItems(0, items);
    for(int i = 0; i < 8; i++)
    {
        ui->m_newList->resizeColumnToContents(i);
    }

    // Cleare the progress bar
    ui->m_progressBar->setRange(0,1);
    ui->m_progressBar->setValue(0);
}

void ManageLibs::downloadReplyFinished(QNetworkReply * a_reply)
{
    TileLib* tileLib = new TileLib(m_tileLibCollection);

    TileLibReader reader(tileLib);

    if (reader.read(a_reply))
    {
        MainWindow* mainWindow = dynamic_cast<MainWindow*>(parent());
        if (mainWindow)
        {
            mainWindow->addLib(tileLib);
            mainWindow->saveLib(tileLib);
        }

        QMessageBox::information(this,tr("Download complete"),tr("Lib downloaded."));
    }
    else
    {
        QMessageBox::information(this,tr("Failed to download"),tr("Failed to decode file"));
    }

    // Cleare the progress bar
    ui->m_progressBar->setRange(0,1);
    ui->m_progressBar->setValue(0);
}

void ManageLibs::replyFinished(QNetworkReply * a_reply)
{

    if (a_reply == m_uploadReply)
    {
        m_uploadReply = NULL;

        uploadReplyFinished(a_reply);
    }

    if (a_reply == m_listReply)
    {
        m_listReply = NULL;

        listReplyFinished(a_reply);
    }

    if (a_reply == m_downloadReply)
    {
        m_downloadReply = NULL;

        downloadReplyFinished(a_reply);
    }

}
