////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2009 Anders Reggestad
// 
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#ifndef DUNGEONMAPSETUP_H_
#define DUNGEONMAPSETUP_H_

#include <QObject>

enum DrawToolType {
    DTT_PAINT,
    DTT_FILL
};

class DungeonMapTileFactoryIf;
class DungeonMapTile;
    
class DungeonMapSetup : public QObject
{
    Q_OBJECT
    
 public:

    static const int GRID_ZVALUE = 0;
    static const int GROUND_TILE_ZVALUE = 1;
    static const int OBJECT_TILE_ZVALUE = 2;
    static const int NOTE_TILE_ZVALUE = 3;
    static const int FOG_OF_WAR_TILE_ZVALUE = 4;
    static const int COMPASS_ZVALUE = 5;

    enum TileSize {
        TILE_SIZE_72_DPI     =   72,
        TILE_SIZE_96_DPI     =   96,
        TILE_SIZE_100_DPI    =  100,
        TILE_SIZE_150_DPI    =  150,
        TILE_SIZE_300_DPI    =  300,
        TILE_SIZE_600_DPI    =  600,
        TILE_SIZE_1200_DPI   = 1200
    };

    DungeonMapSetup();
    virtual ~DungeonMapSetup();

    int tileSize() const;
    void setTileSize( TileSize a_tileSize );

    int width() const;
    void setWidth( int a_width );
    
    int height() const;
    void setHeight( int a_height );
    
    DungeonMapTileFactoryIf* tileFactory();
    void setTileFactory( DungeonMapTileFactoryIf* a_factory );
    
    DungeonMapTile* currentTile();
    void setCurrentTile( DungeonMapTile* a_tile );

    DrawToolType currentTool() const;
    void setCurrentTool( DrawToolType a_tool );

    bool checkTileSizeAtPaint() const;
    void setCheckTileSizeAtPaint( bool a_checkTileSizeAtPaint);

    const QString& serverUrl() const;


 signals:
    void prepareSizeChange();
    
 private:
    int m_tileSize;
    int m_width;
    int m_height;

    DungeonMapTileFactoryIf* m_tileFactory;
    DungeonMapTile*          m_currentTile;

    DrawToolType             m_currentTool;
    
    bool                     m_checkTileSizeAtPaint;

    QString                  m_serverUrl;
};


#endif
