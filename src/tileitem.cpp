////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2009 Anders Reggestad
// 
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#include <QPainter>
#include <QMenu>
#include <QDebug>
#include <QGraphicsSceneContextMenuEvent>
#include <QGraphicsScene>
#include "tileitem.h"
#include "dungeonmapscene.h"
#include "commands.h"


TileItem::TileItem( const DungeonMapTile* a_tile, int a_posX, int a_posY, int a_tileSize, int a_rotate )
{
    Q_ASSERT( a_tile != NULL );

    setFlag( QGraphicsItem::ItemIsSelectable, true );
    
    m_tile = a_tile;
    m_posX = a_posX;
    m_posY = a_posY;
    m_tileSize = a_tileSize;
    setRotate( a_rotate );
}


TileItem::~TileItem()
{    
}

    
QRectF TileItem::boundingRect() const
{
    return QRectF( m_posX*m_tileSize , m_posY*m_tileSize ,
                   m_tile->width()*m_tileSize, m_tile->height()*m_tileSize );  
}


void TileItem::paint( QPainter* a_painter, const QStyleOptionGraphicsItem* a_option,
                      QWidget* a_widget)
{
    Q_UNUSED( a_option );
    Q_UNUSED( a_widget );

    DungeonMapScene* mapScene = dynamic_cast<DungeonMapScene*>(scene());

    if (m_tile->isNoteTile())
    {
        if (!mapScene->viewNotes()) return;
    }

    QRectF tileRect( m_posX*m_tileSize , m_posY*m_tileSize , m_tile->width()*m_tileSize, m_tile->height()*m_tileSize );

    if ( isGroundTile() )
    {

        a_painter->drawImage( tileRect, m_tile->image() );

        //TODO: Add blending with neigbour tiles.
    }
    else
    {
        a_painter->drawImage( tileRect, m_tile->image() );
    }

    if (isSelected())
    {
        a_painter->setPen( QColor( 255, 255, 255 ) );
        a_painter->drawRect( tileRect );
    }
}

bool TileItem::isGroundTile() const
{
    return m_tile->isGroundTile();
}

bool TileItem::isObjectTile() const
{
    return m_tile->isObjectTile();
}

bool TileItem::isNoteTile() const
{
    return m_tile->isNoteTile();
}

bool TileItem::isFogOfWarTile() const
{
    return m_tile->isFogOfWarTile();
}

const DungeonMapTile* TileItem::tile() const
{
    return m_tile;
}

int TileItem::posX() const
{
    return m_posX;
}

int TileItem::posY() const
{
    return m_posY;
}

int TileItem::tileSize() const
{
    return m_tileSize;
}

int TileItem::rotate() const
{
    return m_rotate;
}

void TileItem::contextMenuEvent( QGraphicsSceneContextMenuEvent* a_event )
{
    if (isGroundTile())
    {
        a_event->ignore();
    }
    else
    {
        a_event->accept();

        QMenu menu;
        menu.addAction( tr("Remove") );
        menu.addAction( tr("Rotate Right") );
        menu.addAction( tr("Rotate Left") );

        QAction * action = menu.exec(a_event->screenPos());
        if (action != NULL)
        {
            DungeonMapScene* mapScene = dynamic_cast<DungeonMapScene*>(scene());

            if ( action->text() == tr("Remove" ) )
            {
                mapScene->undoStack()->push( new RemoveItemTileCommand( mapScene, this ));
            }
            else if ( action->text() == tr("Rotate Right" ) )
            {
                mapScene->undoStack()->push( new RotateItemTileCommand( mapScene, this, m_rotate + 90));
            }
            else if ( action->text() == tr("Rotate Left" ) )
            {
                mapScene->undoStack()->push( new RotateItemTileCommand( mapScene, this, m_rotate - 90));
            }
        }
    }
}

void TileItem::setRotate( int a_rotate )
{
    m_rotate = a_rotate;

    qreal x = (m_posX+0.5)*m_tileSize;
    qreal y = (m_posY+0.5)*m_tileSize;
    
    setTransform(QTransform().translate(x, y).rotate(m_rotate).translate(-x, -y));
}
