CONFIG += static
CONFIG += qt

QT += xml
QT += widgets
QT += printsupport
QT += network

HEADERS += \
    mainwindow.h \
    newdungeonmap.h \
    mapwidget.h \
    griditem.h \
    tileitem.h \
    dungeonmapscene.h \
    dungeonmapsetup.h \
    dungeonmaptile.h \
    mapwriter.h \
    mapreader.h \
    compassitem.h \
    commands.h \
    newtilelib.h \
    tilelibreader.h \
    tilelibwriter.h \
    tilelib.h \
    tilelibcollection.h \
    newtile.h \
    managelibs.h \
    tilelibsummaryreader.h

SOURCES += \
    main.cpp \
    mainwindow.cpp \
    newdungeonmap.cpp \
    mapwidget.cpp \
    griditem.cpp \
    tileitem.cpp \
    dungeonmapscene.cpp \
    dungeonmapsetup.cpp \
    dungeonmaptile.cpp \
    mapwriter.cpp \
    mapreader.cpp \
    compassitem.cpp \
    commands.cpp \
    newtilelib.cpp \
    tilelibreader.cpp \
    tilelibwriter.cpp \
    tilelib.cpp \
    tilelibcollection.cpp \
    newtile.cpp \
    managelibs.cpp \
    tilelibsummaryreader.cpp

FORMS += \
    newdungeonmap.ui \
    newtilelib.ui \
    newtile.ui \
    managelibs.ui

RESOURCES     = dungeonmapper.qrc

MOC_DIR = moc
RCC_DIR = rcc
UI_DIR = ui
OBJECTS_DIR = i386

TARGET = dungeonmapper

QMAKE_CXXFLAGS_RELEASE += -DQT_NO_DEBUG_OUTPUT
win32:LIBS += -lAdvapi32

images.path  = ../release/dungeonmapper/images
images.files = ../images/*

tilelibs.path  = ../release/dungeonmapper/libs
tilelibs.files = ../libs/standard.xml

target.path  = ../release/dungeonmapper/bin/

copying.files = COPYING
copying.path  = ../release/dungeonmapper/

examples.files = ../examples/*
examples.path  = ../release/dungeonmapper/examples

INSTALLS += images tilelibs target copying examples
