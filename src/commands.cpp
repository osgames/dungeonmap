////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2013 Anders Reggestad
//
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#include "commands.h"

AddGroundTileCommand::AddGroundTileCommand(DungeonMapScene *a_scene, TileItem* a_prevTileItem, const DungeonMapTile* a_tile,
                                           int a_posX, int a_posY, int a_tileSize, QUndoCommand *parent) :
    QUndoCommand(a_scene->tr("Brush"), parent),
    m_scene(a_scene),
    m_prevTileItem(a_prevTileItem)
{
    m_newTileItem = new TileItem( a_tile, a_posX, a_posY, a_tileSize );
    m_newTileItem->setZValue( DungeonMapSetup::GROUND_TILE_ZVALUE );
    m_scene->addItem(m_newTileItem);
}

void AddGroundTileCommand::undo()
{
    if (m_prevTileItem) m_prevTileItem->setVisible(true);
    m_newTileItem->setVisible(false);
}

void AddGroundTileCommand::redo()
{
    if (m_prevTileItem) m_prevTileItem->setVisible(false);
    m_newTileItem->setVisible(true);
}

AddItemTileCommand::AddItemTileCommand(DungeonMapScene *a_scene, const DungeonMapTile* a_tile,
                                       int a_posX, int a_posY, int a_tileSize, QUndoCommand *parent) :
    QUndoCommand(a_scene->tr("Insert"), parent),
    m_scene(a_scene)
{
    m_newTileItem = new TileItem( a_tile, a_posX, a_posY, a_tileSize );
    if (a_tile->isObjectTile())
    {
       m_newTileItem->setZValue( DungeonMapSetup::OBJECT_TILE_ZVALUE );
    }
    else if (a_tile->isNoteTile())
    {
       m_newTileItem->setZValue( DungeonMapSetup::NOTE_TILE_ZVALUE );
    }
}

void AddItemTileCommand::undo()
{
    m_scene->removeTileItem( m_newTileItem );
}

void AddItemTileCommand::redo()
{
    m_scene->addTileItem( m_newTileItem );
}

RemoveItemTileCommand::RemoveItemTileCommand(DungeonMapScene *a_scene, TileItem* a_item, QUndoCommand *parent) :
    QUndoCommand(a_scene->tr("Remove"), parent),
    m_scene(a_scene),
    m_item(a_item)
{
}

void RemoveItemTileCommand::undo()
{
    m_scene->addTileItem( m_item );
}

void RemoveItemTileCommand::redo()
{
     m_scene->removeTileItem( m_item );
}

RotateItemTileCommand::RotateItemTileCommand(DungeonMapScene *a_scene, TileItem* a_item, int a_rotate, QUndoCommand *parent) :
    QUndoCommand(a_scene->tr("Rotate"), parent),
    m_scene(a_scene),
    m_item(a_item),
    m_newRotate(a_rotate),
    m_prevRotate(a_item->rotate())
{
}

void RotateItemTileCommand::undo()
{
    m_item->setRotate(m_prevRotate);
}

void RotateItemTileCommand::redo()
{
    m_item->setRotate(m_newRotate);
}

