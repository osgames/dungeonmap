////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2013 Anders Reggestad
//
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#ifndef TILELIBCOLLECTION_H
#define TILELIBCOLLECTION_H

#include <QStackedWidget>
#include <QComboBox>
#include <QDateTime>

#include "tilelib.h"

class TileLibCollection : public DungeonMapTileFactoryIf
{
public:
    TileLibCollection();
    virtual ~TileLibCollection();

    int getNextTileId();

    void addTile(DungeonMapTile* a_tile);
    void addTileLib(TileLib* a_tileLib);

    virtual DungeonMapTile* getTile( int a_id );
    virtual DungeonMapTile* getTile( const QString& a_uniqueId );
    virtual DungeonMapTile* getTile( const QUuid& a_uuid );

    QList<TileLib*>& map();

    TileLib* getTileLib(const QUuid& a_uuid );
    TileLib* getTileLib(const QString& name );

    void setDockWidgets(DungeonMapTile::TileType a_tileType, QStackedWidget* a_stackedWidget, QComboBox* a_pageWidget);
    QStackedWidget* getStackedWidget(DungeonMapTile::TileType a_tileType);
    QComboBox* getPageWidget(DungeonMapTile::TileType a_tileType);
private:
    int m_nextTileId;

    QList<TileLib*>                 m_tileLibs;
    QMap<QUuid, TileLib*>           m_tileLibsByUuid;
    QMap<QString, TileLib*>         m_tileLibsByName;

    QMap<int,     DungeonMapTile*>  m_tilesById;
    QMap<QUuid,   DungeonMapTile*>  m_tilesByUuid;
    QMap<QString, DungeonMapTile* > m_tilesByUniqueId;

    QMap<int, QStackedWidget*> m_stackedWidget;
    QMap<int, QComboBox*>      m_pageWidget;
};

#endif // TILELIBCOLLECTION_H
