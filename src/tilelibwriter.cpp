////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2013 Anders Reggestad
//
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#include <QBuffer>

#include "tilelibwriter.h"

TileLibWriter::TileLibWriter(TileLib* a_lib ):
    m_lib(a_lib)
{
    setAutoFormatting(true);
}

bool TileLibWriter::writeFile( QIODevice* a_device )
{
    setDevice( a_device );

    writeStartDocument();
    writeDTD("<!DOCTYPE tilelib>");
    writeStartElement("tilelib");
    writeAttribute("version", "1.2");
    writeAttribute("uuid",m_lib->uuid().toString());
    writeAttribute("domain",m_lib->domain());
    writeAttribute("name",m_lib->name());
    writeAttribute("created",m_lib->created().toString(Qt::ISODate));
    writeAttribute("author",m_lib->author());
    if (!m_lib->modifier().isEmpty())
    {
        writeAttribute("modified",m_lib->modified().toString(Qt::ISODate));
        writeAttribute("modifier",m_lib->modifier());
    }
    writeAttribute("tile_size",QString::number(m_lib->tileSize()));
    writeAttribute("includes_ground_tile",m_lib->includesGroundTiles()?"true":"false");
    writeAttribute("includes_object_tile",m_lib->includesObjectTiles()?"true":"false");
    writeAttribute("includes_note_tile",m_lib->includesNoteTiles()?"true":"false");
    writeAttribute("includes_fog_of_war_tile",m_lib->includesFogOfWarTiles()?"true":"false");

    writeTiles();

    writeEndDocument();
    return true;
}


void TileLibWriter::writeTiles()
{
    writeStartElement("tiles");

    foreach (DungeonMapTile* tile, m_lib->tilesById())
    {
        writeTile(tile);
    }

    writeEndElement();
}

void TileLibWriter::writeTile(DungeonMapTile* tile)
{
    writeStartElement("tile");
    writeAttribute("name",tile->name());
    writeAttribute("uuid",tile->uuid().toString());
    if (!tile->uniqueId().isEmpty())
    {
       writeAttribute("unique_id",tile->uniqueId());
    }
    writeAttribute("type",tile->typeName());
    writeAttribute("width",QString::number(tile->width()));
    writeAttribute("height",QString::number(tile->height()));

    // Save the image as a png into a byte array.
    QByteArray imageData;
    QBuffer buffer(&imageData);
    tile->image().save(&buffer, "PNG");

    writeStartElement("image");
    writeCDATA(imageData.toBase64());
    writeEndElement();

    writeEndElement();
}

