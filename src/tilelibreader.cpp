////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2013 Anders Reggestad
//
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#include <QBuffer>
#include "tilelibreader.h"

TileLibReader::TileLibReader(TileLib* a_tileLib):
    m_tileLib(a_tileLib)
{
}

bool TileLibReader::read( QIODevice* a_device )
{
    setDevice( a_device );

    while (!atEnd())
    {
        readNext();

        if (isStartElement())
        {
            if (name() == "tilelib")
            {
                m_version = attributes().value("version").toString().toDouble();

                if ((m_version == 1.0) || (m_version == 1.1) || (m_version == 1.2))
                {
                    // Changes from version 1.0 to 1.1:
                    // -Images changed to be stored inline.
                    // Changes from 1.1 to 1.2:
                    // -Changed inline stream format from streamd QImages to saved PNG images.
                    // -Removed all save of image attribute (File name) for tiles.

                    readTileLib();
                }
                else
                {
                    raiseError(QObject::tr("This Dungeon Mapper Tile Lib is of unkonwn version."));
                }
            }
            else
            {
                raiseError(QObject::tr("The file is not an Dungeon Mapper Tile Lib file."));
            }
        }
    }

    return !error();
}

bool TileLibReader::boolAttribute(const QString& a_name, bool a_default)
{
    if (attributes().hasAttribute(a_name))
    {
        QString value = attributes().value(a_name).toString().toUpper();
        return (value == "TRUE" || value == "YES");
    }
    return a_default;
}

void TileLibReader::readTileLib()
{
    Q_ASSERT(isStartElement() && name() == "tilelib" );

    QString libDomain = attributes().value("domain").toString();
    QString libName = attributes().value("name").toString();

    if (attributes().hasAttribute("uuid"))
    {
        m_tileLib->setUuid(QUuid(attributes().value("uuid").toString()));
    }
    else
    {
        m_tileLib->setUuid(QUuid::createUuid());
    }

    QString author = attributes().value("author").toString();
    m_tileLib->setAuthor(author);
    QDateTime created = QDateTime::fromString(attributes().value("created").toString(),Qt::ISODate);
    m_tileLib->setCreated(created);

    if (attributes().hasAttribute("modifier"))
    {
        m_tileLib->setModifier(attributes().value("modifier").toString());
    }
    if (attributes().hasAttribute("modified"))
    {
        m_tileLib->setModified(QDateTime::fromString(attributes().value("modified").toString(),Qt::ISODate));
    }

    m_tileLib->setDomain(libDomain);
    m_tileLib->setName(libName);
    m_tileLib->setTileSize(attributes().value("tile_size").toString().toInt());
    bool groundTiles = boolAttribute("includes_ground_tile",true);
    bool objectTiles = boolAttribute("includes_object_tile",true);
    bool noteTiles = boolAttribute("includes_note_tile",true);
    bool fogOfWarTiles = boolAttribute("includes_fog_of_war_tile",true);
    m_tileLib->setLibsToInclude(groundTiles,objectTiles,noteTiles,fogOfWarTiles);

    while ( !atEnd() )
    {
        readNext();

        if ( isEndElement() )
            break;

        if ( isStartElement() )
        {
            if (name() == "tiles")
            {
                readTiles();
            }
            else
            {
                readUnknownElement();
            }
         }
     }

}

void TileLibReader::readTiles()
{
    Q_ASSERT( isStartElement() && name() == "tiles" );

    while ( !atEnd() )
    {
        readNext();

        if ( isEndElement() )
            break;

        if ( isStartElement() )
        {
            if (name() == "tile")
            {
                readTile();
            }
            else
            {
                readUnknownElement();
            }
        }
    }
}

void TileLibReader::readTile()
{
    Q_ASSERT( isStartElement() && name() == "tile" );

    QString tileName = attributes().value("name").toString();
    // Load or create a UUID for the tile.
    QUuid uuid;
    if (attributes().hasAttribute("uuid"))
    {
        uuid = QUuid(attributes().value("uuid").toString());
    }
    else
    {
        uuid = QUuid::createUuid();
    }
    QString uniqueId = attributes().value("unique_id").toString();
    QString typeName = attributes().value("type").toString();
    DungeonMapTile::TileType tileType = DungeonMapTile::stringToTileType(typeName);
    int width = attributes().value("width").toString().toInt();
    int height = attributes().value("height").toString().toInt();

    // Version 1.0 only stored tiles using file names. From 1.1 the tile has
    // been inlined into the library file. Though keep reading images.
    QImage image;
    if (attributes().hasAttribute("image"))
    {
        QString imageName = attributes().value("image").toString();
        image = QImage(imageName);
        m_tileLib->setDirty(true); // Force save of lib to convert to new format.
    }
    else
    {
        // From version 1.1 the image inlined as a sub element of the tile element.
        while ( !atEnd() )
        {
            readNext();

            if ( isEndElement() )
                break;

            if ( isStartElement() )
            {
                if (name() == "image")
                {
                    image = readImage();
                }
                else
                {
                    readUnknownElement();
                }
            }
        }
    }
    // Add tile but do not update the dirty flag
    m_tileLib->addTile(new DungeonMapTile(tileName, uuid, uniqueId, image, tileType, width, height), false);
}

QImage TileLibReader::readImage()
{
    Q_ASSERT( isStartElement() && name() == "image" );
    QImage image;

    while ( !atEnd() )
    {
        readNext();

        if ( isEndElement() )
            break;

        if (isCharacters())
        {
            QByteArray imageData = QByteArray::fromBase64(QByteArray(text().toUtf8()));
            QBuffer buffer(&imageData);
            if (!image.load(&buffer,"PNG"))
            {
                // If image failed to load as png try image stream that was used in version 1.1 and older.
                QDataStream stream(&imageData,QIODevice::ReadOnly);
                stream >> image;
                m_tileLib->setDirty(true); // Force save of lib to convert to new format.
            }
        }
    }
    return image;
}

void TileLibReader::readUnknownElement()
{
    Q_ASSERT( isStartElement() );

    while ( !atEnd() )
    {
        readNext();

        if ( isEndElement() )
            break;

        if ( isStartElement() )
        {
            readUnknownElement();
        }
     }
 }
