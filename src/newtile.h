////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2013 Anders Reggestad
//
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#ifndef NEWTILE_H
#define NEWTILE_H

#include <QDialog>
#include <QStatusBar>

#include "dungeonmaptile.h"
#include "tilelibcollection.h"

namespace Ui {
class NewTile;
}

class NewTile : public QDialog
{
    Q_OBJECT
    
public:
    explicit NewTile(TileLibCollection* a_tileLibCollection, QStatusBar* a_statusBar, QWidget *parent = 0);
    ~NewTile();

    static bool checkIfLibIsModifiable(TileLib* tileLib);
    static bool checkForLibs(TileLibCollection* a_tileLibCollection);

    void addTileLib(const QString& a_name, const QUuid& a_uuid);

    QUuid tileLib() const;
    QString tileName() const;
    QString imageName() const;
    DungeonMapTile::TileType tileType() const;
    int tileWidth() const;
    int tileHeight() const;

private slots:
    void changedLib(int index);
    void browsImage();
    void imageNameChanged(QString fileName);
    
private:

    virtual void done(int r);

    Ui::NewTile *ui;
    TileLibCollection *m_tileLibCollection;
    QStatusBar* m_statusBar;
};

#endif // NEWTILE_H
