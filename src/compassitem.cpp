////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2013 Anders Reggestad
//
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#include <QPainter>
#include <QDebug>

#include "compassitem.h"
#include "dungeonmapsetup.h"

CompassItem::CompassItem( DungeonMapSetup* a_setup, const QString& a_imageName )
{
    m_setup = a_setup;
    m_image = QImage( a_imageName );

    connect( m_setup, SIGNAL( prepareSizeChange() ), this, SLOT( prepareSizeChange() ));

    setZValue( DungeonMapSetup::COMPASS_ZVALUE );
}


CompassItem::~CompassItem()
{

}

void CompassItem::prepareSizeChange()
{
    prepareGeometryChange ();
}

QRectF CompassItem::boundingRect() const
{
    int t = m_setup->tileSize();
    int w = m_setup->width()*t;
    int h = m_setup->height()*t;

    return QRectF( w-m_image.width() , h-m_image.height() , m_image.width(), m_image.height() );
}

void CompassItem::paint(QPainter* a_painter, const QStyleOptionGraphicsItem* a_option, QWidget* a_widget)
{
    Q_UNUSED( a_option );
    Q_UNUSED( a_widget );

    QRectF compassRect = boundingRect();

    a_painter->drawImage( compassRect, m_image );
}

