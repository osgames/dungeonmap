////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2009 Anders Reggestad
// 
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#include <Qt>
#include "dungeonmapsetup.h"

DungeonMapSetup::DungeonMapSetup()
{
    m_tileSize = TILE_SIZE_72_DPI;
    m_width = 75;
    m_height = 50;
    m_tileFactory = NULL;
    m_currentTile = NULL;
    m_currentTool = DTT_PAINT;
    m_checkTileSizeAtPaint = true;
    m_serverUrl = "http://dungeonmap.sourceforge.net";
}

DungeonMapSetup::~DungeonMapSetup()
{
    
}

int DungeonMapSetup::tileSize() const
{
    return m_tileSize;
}

void DungeonMapSetup::setTileSize(TileSize a_tileSize)
{
    m_tileSize = a_tileSize;
}

int DungeonMapSetup::width() const
{
    return m_width;
}

void DungeonMapSetup::setWidth( int a_width )
{
    if ( m_width != a_width )
    {
        emit prepareSizeChange();

        m_width = a_width;
    }
}

int DungeonMapSetup::height() const
{
    return m_height;
}

void DungeonMapSetup::setHeight( int a_height )
{
    if ( m_height != a_height )
    {
        emit prepareSizeChange();

        m_height = a_height;
    }
}

DungeonMapTileFactoryIf* DungeonMapSetup::tileFactory()
{
    return m_tileFactory;
}

void DungeonMapSetup::setTileFactory( DungeonMapTileFactoryIf* a_factory )
{
    m_tileFactory = a_factory;
}

DungeonMapTile* DungeonMapSetup::currentTile()
{
    return m_currentTile;
}

void DungeonMapSetup::setCurrentTile( DungeonMapTile* a_tile )
{
    m_currentTile = a_tile;
}

DrawToolType DungeonMapSetup::currentTool() const
{
    return m_currentTool;
}

void DungeonMapSetup::setCurrentTool( DrawToolType a_tool )
{
    m_currentTool = a_tool;
}

bool DungeonMapSetup::checkTileSizeAtPaint() const
{
    return m_checkTileSizeAtPaint;
}

void DungeonMapSetup::setCheckTileSizeAtPaint(bool a_checkTileSizeAtPaint)
{
    m_checkTileSizeAtPaint = a_checkTileSizeAtPaint;
}

const QString& DungeonMapSetup::serverUrl() const
{
    return m_serverUrl;
}
