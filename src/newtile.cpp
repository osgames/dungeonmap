////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2013 Anders Reggestad
//
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#include <QFileDialog>
#include <QPicture>

#include "newtile.h"
#include "ui_newtile.h"

NewTile::NewTile(TileLibCollection* a_tileLibCollection, QStatusBar* a_statusBar, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewTile),
    m_tileLibCollection(a_tileLibCollection),
    m_statusBar(a_statusBar)
{
    ui->setupUi(this);

    foreach (TileLib* tileLib, m_tileLibCollection->map())
    {
        if (checkIfLibIsModifiable(tileLib))
        {
           addTileLib(tileLib->name(),tileLib->uuid());
        }
    }
    // Call change lib to set up tile types available for this lib
    changedLib(ui->m_lib->currentIndex());
    ui->m_tileType->setCurrentIndex(0);

    ui->m_width->setValue(1);
    ui->m_height->setValue(1);

    connect( ui->m_lib, SIGNAL(activated(int)), this, SLOT(changedLib(int)));
    connect( ui->m_browsImage, SIGNAL(clicked()), this, SLOT(browsImage()));
    connect( ui->m_imageName, SIGNAL(textChanged(QString)), this, SLOT(imageNameChanged(QString)));
}

NewTile::~NewTile()
{
    delete ui;
}

bool NewTile::checkIfLibIsModifiable(TileLib* tileLib)
{
    // Only allow modification of the build inn standard dungeon mapper lib.
    return (tileLib->domain() != "Dungeon mapper" && tileLib->name() != "Standard");
}

bool NewTile::checkForLibs(TileLibCollection* a_tileLibCollection)
{
    foreach (TileLib* tileLib, a_tileLibCollection->map())
    {
        if (checkIfLibIsModifiable(tileLib))
        {
           return true; // Found one modifiable lib.
        }
    }
    return false; // Found no libs to modify.
}

void NewTile::addTileLib(const QString &a_name, const QUuid &a_uuid)
{
    ui->m_lib->addItem( a_name, QVariant(a_uuid));
}

QUuid NewTile::tileLib() const
{
    return ui->m_lib->itemData(ui->m_lib->currentIndex()).toUuid();
}

QString NewTile::tileName() const
{
    return ui->m_name->text();
}

QString NewTile::imageName() const
{
    return ui->m_imageName->text();
}

DungeonMapTile::TileType NewTile::tileType() const
{
    int value = ui->m_tileType->itemData(ui->m_tileType->currentIndex()).toInt();

    return (DungeonMapTile::TileType)value;
}

int NewTile::tileWidth() const
{
    return ui->m_width->value();
}

int NewTile::tileHeight() const
{
    return ui->m_height->value();
}

void NewTile::changedLib(int index)
{
    Q_UNUSED(index)

    TileLib* lib = m_tileLibCollection->getTileLib(tileLib());
    if (lib)
    {
        ui->m_tileType->clear();
        if (lib->includesGroundTiles())
        {
            ui->m_tileType->addItem("Ground tile", QVariant(DungeonMapTile::GROUND_TILE));
        }
        if (lib->includesObjectTiles())
        {
            ui->m_tileType->addItem("Item tile", QVariant(DungeonMapTile::OBJECT_TILE));
        }
        if (lib->includesNoteTiles())
        {
            ui->m_tileType->addItem("Note tile", QVariant(DungeonMapTile::NOTE_TILE));
        }
        if (lib->includesFogOfWarTiles())
        {
            ui->m_tileType->addItem("Fog of War tile", QVariant(DungeonMapTile::FOG_OF_WAR_TILE));
        }
    }
}

void NewTile::browsImage()
{
    QString fileName = QFileDialog::getOpenFileName(this,tr("Tile image"),"../images",tr("Image Files (*.png *.jpg *.bmp);;Any files (*.*)"));
    ui->m_imageName->setText(fileName);
}

void NewTile::imageNameChanged(QString fileName)
{
    if (QFile(fileName).exists())
    {
        ui->m_image->setPixmap(QPixmap(fileName));
        QIcon icon(fileName);
        ui->m_icon->setPixmap(icon.pixmap(32));
    }
    else
    {
        ui->m_icon->clear();
        ui->m_image->clear();
    }
}

void NewTile::done(int r)
{
    if(QDialog::Accepted == r)  // ok was pressed
    {
        // validate the data somehow
        if (ui->m_name->text().isEmpty())
        {
            m_statusBar->showMessage(tr("Name is missing."));
            return;
        }

        if (ui->m_imageName->text().isEmpty())
        {
            m_statusBar->showMessage(tr("Image name is missing."));
            return;
        }

        if (!QFile(ui->m_imageName->text()).exists())
        {
            m_statusBar->showMessage(tr("Image name do not exist."));
            return;
        }

        if (!ui->m_image->pixmap() || ui->m_image->pixmap()->isNull())
        {
            m_statusBar->showMessage(tr("No image selected."));
            return;
        }

        QDialog::done(r);
        return;
    }
    else    // cancel, close or exc was pressed
    {
        QDialog::done(r);
        return;
    }
}
