////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2009 Anders Reggestad
// 
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#include <QtGui>
#include <QWidget>
#include <QFileDialog>
#include <QPrinter>
#include <QPrintDialog>
#include <QMessageBox>
#include <QMenuBar>
#include <QToolBar>
#include <QStatusBar>
#include <QDockWidget>
#include <QPrintPreviewDialog>
#include <QComboBox>
#include <QStackedWidget>
#include <QNetworkCookieJar>
#include <QApplication>
#include <QVBoxLayout>

#ifdef WIN32
#else
#include <unistd.h>
#endif

#include "mainwindow.h"
#include "newdungeonmap.h"
#include "mapwriter.h"
#include "mapreader.h"
#include "newtilelib.h"
#include "newtile.h"
#include "tilelib.h"
#include "tilelibcollection.h"
#include "tilelibwriter.h"
#include "tilelibreader.h"
#include "managelibs.h"

const char* MainWindow::NEW_LIB = "New lib..";

MainWindow::MainWindow()
{
    m_networkAccessManager.setCookieJar(new QNetworkCookieJar());
}

MainWindow::MainWindow(const QString& a_fileName)
{
    m_networkAccessManager.setCookieJar(new QNetworkCookieJar());

    DungeonMapSetup* setup = new DungeonMapSetup;
    
    init( setup );
    loadFile( a_fileName );
}

MainWindow::~MainWindow()
{
    delete m_tileLibCollection;
}

void MainWindow::closeEvent(QCloseEvent* a_event)
{
    if (maybeSave())
    {
        writeSettings();
        a_event->accept();
    } else
    {
        a_event->ignore();
    }
}

void MainWindow::newFile()
{
    DungeonMapSetup* setup = NewDungeonMap::getSetup( this );
    if ( setup )
    {
        MainWindow *other = new MainWindow();
        if (other->init(setup))
        {
            other->move(x() + 40, y() + 40);
            other->show();
        }
        else
        {
            delete other;
        }
    }
}

void MainWindow::open()
{
   QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                   "",tr("Dungeon maps (*.dmap)") );
    
   qDebug() <<  "Opening file: " << fileName ;
   openFile(fileName);
}

void MainWindow::openFile(const QString& a_fileName)
{
   if (!a_fileName.isEmpty())
   {
      MainWindow *existing = findMainWindow( a_fileName );
      if (existing)
      {
         existing->show();
         existing->raise();
         existing->activateWindow();
         return;
      }
        
      if ( isUntitled && m_mapWidget->isEmpty() && !isWindowModified() )
      {
         loadFile(a_fileName);
      }
      else
      {
         MainWindow *other = new MainWindow(a_fileName);
         if (other->isUntitled)
         {
            delete other;
            return;
         }
         other->move(x() + 40, y() + 40);
         other->show();
      }
   }
}

bool MainWindow::save()
{
    if (isUntitled)
    {
        return saveAs();
    }
    else
    {
        return saveFile(curFile);
    }
}

bool MainWindow::saveAs()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save As"),
                                                    curFile,
                                                    tr("Dungeon maps (*.dmap)") );
    if (fileName.isEmpty())
    {
        return false;
    }

    return saveFile(fileName);
}

void MainWindow::openRecentFile()
{
    QAction *action = qobject_cast<QAction *>(sender());
    if (action)
        openFile(action->data().toString());
}

void MainWindow::print()
{
   QPrinter printer(QPrinter::HighResolution);
   printer.setPaperSize(QPrinter::A4);
   QPrintPreviewDialog preview(&printer, this);
   connect(&preview, SIGNAL(paintRequested(QPrinter*)), SLOT(printPreview(QPrinter*)));
   preview.exec();
}

void MainWindow::printPreview(QPrinter *printer)
{
  QPainter painter(printer);
  painter.setRenderHint(QPainter::Antialiasing);

  int      w = printer->pageRect().width();
  int      h = printer->pageRect().height();

  // create a font appropriate to page size
  QFont    font = painter.font();
  font.setPixelSize( (w+h) / 100 );
  painter.setFont( font );

  QRect    page( 0, 0, w, h );

  QRect mapArea = page;
  // Make some space
  mapArea.adjust( w/20, h/20, -w/20, -h/20 );


  int wMM = printer->pageRect(QPrinter::Millimeter).width();
  int hMM = printer->pageRect(QPrinter::Millimeter).height();
  QRect pageMM(0,0,wMM,hMM);
  pageMM.adjust(wMM/20,hMM/20, -wMM/20, -hMM/20);

  int tilesX = qFloor(pageMM.width()/25.4);
  int tilesY = qFloor(pageMM.height()/25.4);
  int numPagesX = qCeil(75.0/tilesX);
  int numPagesY = qCeil(50.0/tilesY);

  int pageNum = 0;
  int pageCount = 0;
  int numPages = numPagesX*numPagesY;

  if (printer->toPage() != 0) numPages = printer->toPage();
  if (printer->fromPage() != 0) numPages -= (printer->fromPage()-1);

  for (int pageX = 0; pageX < numPagesX; pageX++)
  {
      for (int pageY = 0; pageY < numPagesY; pageY++)
      {
          pageNum++;
          pageCount++;

          if (printer->fromPage() != 0 && pageNum < printer->fromPage()) continue;
          if (printer->toPage() != 0 && pageNum > printer->toPage()) continue;

          // draw labels in corners of page
          painter.drawText( page, Qt::AlignTop    | Qt::AlignLeft, "Dungeon Mapper" );
          painter.drawText( page, Qt::AlignBottom | Qt::AlignCenter, QString::number( pageNum ) );
          painter.drawText( page, Qt::AlignBottom | Qt::AlignRight,
                            QDateTime::currentDateTime().toString( Qt::DefaultLocaleShortDate ) );

          // draw the map
          QRect source = QRect(pageX*tilesX*72,pageY*tilesY*72,tilesX*72,tilesY*72);
          m_mapWidget->scene()->render(&painter, mapArea, source, Qt::KeepAspectRatioByExpanding);

          if (pageCount != numPages) printer->newPage();
      }
  }
  painter.end();
}

void MainWindow::exportMap()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Export"),
                                                    "",
                                                    tr("Images (*.png *.jpeg)") );
    if (fileName.isEmpty())
    {
        return;
    }

    QPixmap pixmap( m_setup->width()*m_setup->tileSize(),
                    m_setup->height()*m_setup->tileSize() );
    QPainter painter(&pixmap);
    painter.setRenderHint(QPainter::Antialiasing);
    m_mapWidget->scene()->render(&painter);
    painter.end();

    pixmap.save(fileName);    
}

void MainWindow::filePrintPdf()
{
    QString fileName = QFileDialog::getSaveFileName(this, "Export PDF",
                                                    QString(), "*.pdf");
    if (!fileName.isEmpty()) {
        if (QFileInfo(fileName).suffix().isEmpty())
            fileName.append(".pdf");
        QPrinter printer(QPrinter::HighResolution);
        printer.setOutputFormat(QPrinter::PdfFormat);
        printer.setOutputFileName(fileName);
        QPainter painter(&printer);
        painter.setRenderHint(QPainter::Antialiasing);
        m_mapWidget->scene()->render(&painter);
        painter.end();
    }
}

void MainWindow::manageLibs()
{
    ManageLibs dlg(&m_networkAccessManager, m_tileLibCollection, statusBar(), m_setup, this);
    dlg.exec();
}

QString MainWindow::libPath() const
{
#ifdef WIN32
     return QDir::cleanPath(QCoreApplication::applicationDirPath() + "/../../libs");
#else
     return QDir::cleanPath(QCoreApplication::applicationDirPath() + "/../libs");
#endif
}


bool MainWindow::saveLibs()
{
    QApplication::setOverrideCursor(Qt::WaitCursor);

    bool saveOk = true;
    bool saved = false;

    foreach(TileLib* tileLib, m_tileLibCollection->map() )
    {
        if (tileLib->isDirty())
        {
            saved = true; // One lib was saved
            if (!saveLib(tileLib))
            {
                saveOk = false;
            }
        }
    }

    if (saved)
    {
        if (saveOk)
        {
            statusBar()->showMessage(tr("Libs saved"), 2000);
        }
        else
        {
            statusBar()->showMessage(tr("Failed to save one or more libs"), 2000);
        }
    }
    else
    {
        statusBar()->showMessage(tr("No modifed libs to save"), 2000);
    }

    QApplication::restoreOverrideCursor();

    return true;
}

bool MainWindow::saveLib(TileLib* a_tileLib)
{
    return saveLib(libPath(), a_tileLib);
}

bool MainWindow::saveLib(const QString& path, TileLib* a_tileLib)
{
    QString fileName = path + QString("/") + a_tileLib->name().toLower() + QString(".xml");

    QFile file( fileName );
    if ( !file.open(QFile::WriteOnly | QFile::Text) )
    {
        QMessageBox::warning( this, tr("Dungeon Mapper"),
                              tr("Cannot write file %1:\n%2.")
                              .arg( fileName )
                              .arg( file.errorString() ) );
        return false;
    }

    TileLibWriter writer( a_tileLib );
    if ( !writer.writeFile( &file ) )
    {
        return false;
    }
    a_tileLib->setDirty(false);
    return true;
}

void MainWindow::addLib(TileLib* a_tileLib)
{
    m_tileLibCollection->addTileLib(a_tileLib);

    createTileLibDockWindow(a_tileLib,DungeonMapTile::GROUND_TILE);
    createTileLibDockWindow(a_tileLib,DungeonMapTile::OBJECT_TILE);
    createTileLibDockWindow(a_tileLib,DungeonMapTile::NOTE_TILE);
    createTileLibDockWindow(a_tileLib,DungeonMapTile::FOG_OF_WAR_TILE);

    newTileAct->setEnabled(NewTile::checkForLibs(m_tileLibCollection));
}


void MainWindow::about()
{
    QMessageBox::about(this, tr("About Dungeon Mapper") + " " + QString::number(MAJOR_VERSION) + "." + QString::number(MINOR_VERSION)+ "." + QString::number(PATCH_LEVEL),
                       tr("DungeonMapper help you draw your dungeons for your next adventure.\n\n"
                          "Copyright (C) 2009 Anders Reggestad\n\n"
                          "DungeonMapper is free software: you can redistribute it and/or modify"
                          " it under the terms of the GNU General Public License as published by"
                          " the Free Software Foundation, either version 3 of the License, or"
                          " (at your option) any later version.\n\n"
                          "DungeonMapper is distributed in the hope that it will be useful,"
                          " but WITHOUT ANY WARRANTY; without even the implied warranty of"
                          " MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the"
                          " GNU General Public License for more details.\n\n"
                          "You should have received a copy of the GNU General Public License"
                          " along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.\n\n"
                          "andersr@pvv.org" ));
}

void MainWindow::documentWasModified()
{
    m_mapWidget->setModified(true);
    setWindowModified(true);
}

void MainWindow::tileSelectionChanged ( QListWidgetItem* a_current, QListWidgetItem* a_previous )
{
    Q_UNUSED( a_previous );


    if ( a_current != NULL )
    {
        // Clear out all selections in all lists. Since we don't
        // easy know where the selected item is.
        clearTileLibSelection();

        m_setup->setCurrentTile( m_tileLibCollection->getTile( a_current->data( m_tileRole ).toInt() ) );
        a_current->setSelected(true);
    }

}

void MainWindow::paintToolSelected()
{
    m_setup->setCurrentTool( DTT_PAINT );
}

void MainWindow::fillToolSelected()
{
    m_setup->setCurrentTool( DTT_FILL );
}


 bool MainWindow::init( DungeonMapSetup* a_setup )
 {
     m_setup = a_setup;
     m_tileLibCollection = new TileLibCollection();
     
     m_tileRole = Qt::UserRole;

     isUntitled = true;

     m_mapWidget = new MapWidget( m_setup );
     setCentralWidget(m_mapWidget);


     createActions();
     createMenus();
     createToolBars();
     createStatusBar();
     createDockWindows();
     
     readSettings();

     connect(m_mapWidget->scene(), SIGNAL(contentsChanged()), this, SLOT(documentWasModified()));
     m_undoGroup->setActiveStack(m_mapWidget->scene()->undoStack());

     setCurrentFile( "" );

     if (!loadLibs()) return false;
     m_setup->setTileFactory( m_tileLibCollection );

     return true;
 }

bool MainWindow::loadLibs()
{
    QApplication::setOverrideCursor(Qt::WaitCursor);

    bool loadOk = true;

    QDirIterator it(QDir::cleanPath(libPath()), QDir::Files, QDirIterator::Subdirectories);
    while (it.hasNext())
    {
        if (!loadLib(it.next()))
        {
            QApplication::restoreOverrideCursor();
            QMessageBox::critical(this,tr("Failed to load"),
                                       tr("Error loading tile lib"), QMessageBox::Close);
            loadOk = false;
            break;
        }
    }
    if (loadOk)
    {
       statusBar()->showMessage(tr("Libs loaded"), 2000);
    }
    QApplication::restoreOverrideCursor();
    return loadOk;
}

bool MainWindow::loadLib(const QString& a_fileName)
{
    QFile file( a_fileName);
    if ( !file.open(QFile::ReadOnly | QFile::Text) )
    {
        QMessageBox::warning( this, tr("Dungeon Mapper"),
                              tr("Cannot read file %1:\n%2.")
                              .arg( a_fileName )
                              .arg( file.errorString() ) );
        return false;
    }

    TileLib* tileLib = new TileLib(m_tileLibCollection);

    TileLibReader reader(tileLib);
    if (!reader.read(&file))
    {
        return false;
    }
    addLib(tileLib);

    return true;
}


 void MainWindow::createActions()
 {
     // File actions

     newAct = new QAction(QIcon(":/icons/filenew.png"), tr("&New"), this);
     newAct->setShortcut(tr("Ctrl+N"));
     newAct->setStatusTip(tr("Create a new file"));
     connect(newAct, SIGNAL(triggered()), this, SLOT(newFile()));

     openAct = new QAction(QIcon(":/icons/fileopen.png"), tr("&Open..."), this);
     openAct->setShortcut(tr("Ctrl+O"));
     openAct->setStatusTip(tr("Open an existing file"));
     connect(openAct, SIGNAL(triggered()), this, SLOT(open()));

     saveAct = new QAction(QIcon(":/icons/filesave.png"), tr("&Save"), this);
     saveAct->setShortcut(tr("Ctrl+S"));
     saveAct->setStatusTip(tr("Save the document to disk"));
     connect(saveAct, SIGNAL(triggered()), this, SLOT(save()));

     saveAsAct = new QAction(QIcon(":/icons/filesaveas.png"), tr("Save &As..."), this);
     saveAsAct->setStatusTip(tr("Save the document under a new name"));
     connect(saveAsAct, SIGNAL(triggered()), this, SLOT(saveAs()));

     for (int i = 0; i < MaxRecentFiles; ++i)
     {
         recentFileActs[i] = new QAction(this);
         recentFileActs[i]->setVisible(false);
         connect(recentFileActs[i], SIGNAL(triggered()),
                 this, SLOT(openRecentFile()));
     }

     printAct = new QAction(QIcon(":/icons/fileprint.png"), tr("&Print..."), this);
     printAct->setShortcut(tr("Ctrl+P"));
     printAct->setStatusTip(tr("Print the Dungeon Map"));
     connect(printAct, SIGNAL(triggered()), this, SLOT(print()));

     exportAct = new QAction(tr("&Export..."), this);
     exportAct->setShortcut(tr("Ctrl+E"));
     exportAct->setStatusTip(tr("Export the Dungeon Map"));
     connect(exportAct, SIGNAL(triggered()), this, SLOT(exportMap()));

     exportPdfAct = new QAction(tr("&Export PDF..."), this);
     exportPdfAct->setShortcut(tr("Ctrl+Alt+E"));
     exportPdfAct->setStatusTip(tr("Export the Dungeon Map to PDF"));
     connect(exportPdfAct, SIGNAL(triggered()), this, SLOT(filePrintPdf()));

     closeAct = new QAction(tr("&Close"), this);
     closeAct->setShortcut(tr("Ctrl+W"));
     closeAct->setStatusTip(tr("Close this window"));
     connect(closeAct, SIGNAL(triggered()), this, SLOT(close()));

     exitAct = new QAction(tr("E&xit"), this);
     exitAct->setShortcut(tr("Ctrl+Q"));
     exitAct->setStatusTip(tr("Exit the application"));
     connect(exitAct, SIGNAL(triggered()), qApp, SLOT(closeAllWindows()));


     // Edit actions
     m_undoGroup = new QUndoGroup(this);
     undoAct = m_undoGroup->createUndoAction(this);
     undoAct->setIcon(QIcon(":/icons/undo.png"));
     undoAct->setShortcut(tr("Ctrl+Z"));

     redoAct = m_undoGroup->createRedoAction(this);
     redoAct->setIcon(QIcon(":/icons/redo.png"));
     redoAct->setShortcut(tr("Shift+Ctrl+Z"));

     paintAct = new QAction(QIcon(":/icons/paintbrush.png"), tr("&Brush"), this);
     paintAct->setShortcut(tr("Ctrl+B"));
     paintAct->setStatusTip(tr("Select the brush paint tool."));
     paintAct->setCheckable( true );
     connect(paintAct, SIGNAL(triggered()), this, SLOT(paintToolSelected()));
     
     fillAct = new QAction(QIcon(":/icons/bucketfill.png"), tr("&Fill"), this);
     fillAct->setShortcut(tr("Ctrl+F"));
     fillAct->setStatusTip(tr("Select the fill paint tool."));
     fillAct->setCheckable( true );
     connect(fillAct, SIGNAL(triggered()), this, SLOT(fillToolSelected()));

     toolActGroup = new QActionGroup( this );
     toolActGroup->addAction( paintAct);
     toolActGroup->addAction( fillAct );
     paintAct->setChecked( true );

     cutAct = new QAction(QIcon(":/icons/editcut.png"), tr("Cu&t"), this);
     cutAct->setShortcut(tr("Ctrl+X"));
     cutAct->setStatusTip(tr("Cut the current selection's contents to the "
                             "clipboard"));
     //     connect(cutAct, SIGNAL(triggered()), textEdit, SLOT(cut()));

     copyAct = new QAction(QIcon(":/icons/editcopy.png"), tr("&Copy"), this);
     copyAct->setShortcut(tr("Ctrl+C"));
     copyAct->setStatusTip(tr("Copy the current selection's contents to the "
                              "clipboard"));
     //     connect(copyAct, SIGNAL(triggered()), textEdit, SLOT(copy()));

     pasteAct = new QAction(QIcon(":/icons/editpaste.png"), tr("&Paste"), this);
     pasteAct->setShortcut(tr("Ctrl+V"));
     pasteAct->setStatusTip(tr("Paste the clipboard's contents into the current "
                               "selection"));
     //     connect(pasteAct, SIGNAL(triggered()), textEdit, SLOT(paste()));

     // View actions
     noteAct = new QAction(QIcon(":/icons/notes.png"),tr("Notes"), this);
     noteAct->setStatusTip(tr("Toggle display of GM notes in map"));
     noteAct->setCheckable(true);
     connect(noteAct, SIGNAL(toggled(bool)), m_mapWidget, SLOT(toggleNotes(bool)));
     noteAct->setChecked(true);

     compassAct = new QAction(QIcon(":/icons/compass.png"), tr("Compass"), this);
     compassAct->setStatusTip(tr("Toggle display of compass in map"));
     compassAct->setCheckable(true);
     connect(compassAct, SIGNAL(toggled(bool)), m_mapWidget, SLOT(toggleCompass(bool)));
     compassAct->setChecked(true);

     // Lib actions
     manageLibsAct = new QAction(tr("Manage Libs..."), this);
     manageLibsAct->setStatusTip(tr("Manage libs"));
     connect(manageLibsAct, SIGNAL(triggered()), this, SLOT(manageLibs()));

     saveLibsAct = new QAction(tr("Save Libs"), this);
     saveLibsAct->setStatusTip(tr("Save all libs"));
     connect(saveLibsAct, SIGNAL(triggered()), this, SLOT(saveLibs()));

     newLibAct = new QAction(tr("New Lib..."), this);
     newLibAct->setStatusTip(tr("Create a new lib"));
     connect(newLibAct, SIGNAL(triggered()), this, SLOT(newLib()));

     newTileAct = new QAction(tr("New tile..."), this);
     newTileAct->setStatusTip(tr("Create a new tile"));
     connect(newTileAct, SIGNAL(triggered()), this, SLOT(newTile()));

     // Help actions
     aboutAct = new QAction(tr("&About"), this);
     aboutAct->setStatusTip(tr("Show the application's About box"));
     connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));

     cutAct->setEnabled(false);
     copyAct->setEnabled(false);
     /*
     connect(textEdit, SIGNAL(copyAvailable(bool)),
             cutAct, SLOT(setEnabled(bool)));
     connect(textEdit, SIGNAL(copyAvailable(bool)),
             copyAct, SLOT(setEnabled(bool)));
     */

 }

 void MainWindow::createMenus()
 {
     fileMenu = menuBar()->addMenu(tr("&File"));
     fileMenu->addAction(newAct);
     fileMenu->addAction(openAct);
     fileMenu->addAction(saveAct);
     fileMenu->addAction(saveAsAct);
     separatorAct = fileMenu->addSeparator();
     for (int i = 0; i < MaxRecentFiles; ++i)
         fileMenu->addAction(recentFileActs[i]);
     fileMenu->addSeparator();
     fileMenu->addAction(printAct);
     fileMenu->addAction(exportAct);
     fileMenu->addAction(exportPdfAct);
     fileMenu->addSeparator();
     fileMenu->addAction(closeAct);
     fileMenu->addAction(exitAct);
     updateRecentFileActions();

     editMenu = menuBar()->addMenu(tr("&Edit"));
     editMenu->addAction(undoAct);
     editMenu->addAction(redoAct);
     editMenu->addSeparator();
     editMenu->addAction(paintAct);
     editMenu->addAction(fillAct);
     /*
     editMenu->addSeparator();
     editMenu->addAction(cutAct);
     editMenu->addAction(copyAct);
     editMenu->addAction(pasteAct);
     */

     viewMenu = menuBar()->addMenu(tr("&View"));
     viewMenu->addAction(noteAct);
     viewMenu->addAction(compassAct);
     viewMenu->addSeparator();

     libMenu = menuBar()->addMenu(tr("&Lib"));
     libMenu->addAction(manageLibsAct);
     libMenu->addAction(saveLibsAct);
     libMenu->addAction(newLibAct);
     libMenu->addAction(newTileAct);

     menuBar()->addSeparator();

     helpMenu = menuBar()->addMenu(tr("&Help"));
     helpMenu->addAction(aboutAct);
 }

 void MainWindow::createToolBars()
 {
     fileToolBar = addToolBar(tr("File"));
     fileToolBar->addAction(newAct);
     fileToolBar->addAction(openAct);
     fileToolBar->addAction(saveAct);

     editToolBar = addToolBar(tr("Edit"));
     editToolBar->addAction(undoAct);
     editToolBar->addAction(redoAct);
     editToolBar->addSeparator();
     editToolBar->addAction(paintAct);
     editToolBar->addAction(fillAct);

     viewToolBar = addToolBar(tr("View"));
     viewToolBar->addAction(noteAct);
     viewToolBar->addAction(compassAct);

     /*
     editToolBar->addSeparator();
     editToolBar->addAction(cutAct);
     editToolBar->addAction(copyAct);
     editToolBar->addAction(pasteAct);
     */
 }

 void MainWindow::createStatusBar()
 {
     statusBar()->showMessage(tr("Ready"));
 }

void MainWindow::createDockWindows()
{
    createTileLibDockWindow(tr("Ground Tiles"), DungeonMapTile::GROUND_TILE);
    createTileLibDockWindow(tr("Object Tiles"), DungeonMapTile::OBJECT_TILE);
    createTileLibDockWindow(tr("Note Tiles"), DungeonMapTile::NOTE_TILE);
//    createTileLibDockWindow(tr("Fog of War Tiles"), DungeonMapTile::FOG_OF_WAR_TILE);
}

void MainWindow::createTileLibDockWindow(const QString & a_title, DungeonMapTile::TileType a_tileType)
{
    QDockWidget *dock = new QDockWidget(a_title, this);

    QStackedWidget* stackedTileLibs = new QStackedWidget();
    QComboBox* pageComboBox = new QComboBox();

    m_tileLibCollection->setDockWidgets(a_tileType, stackedTileLibs, pageComboBox);

    foreach (TileLib* tileLib, m_tileLibCollection->map())
    {
        createTileLibDockWindow(tileLib, a_tileType);
    }

    connect(pageComboBox, SIGNAL(activated(int)),
            stackedTileLibs, SLOT(setCurrentIndex(int)));

    QVBoxLayout* layoutGroundTiles = new QVBoxLayout();
    layoutGroundTiles->addWidget(pageComboBox);
    layoutGroundTiles->addWidget(stackedTileLibs);
    QWidget* panelTiles = new QWidget();
    panelTiles->setLayout(layoutGroundTiles);

    dock->setWidget( panelTiles );

    addDockWidget( Qt::RightDockWidgetArea, dock );
    viewMenu->addAction( dock->toggleViewAction() );

    pageComboBox->setCurrentText(tr("Standard"));
    pageComboBox->activated(pageComboBox->currentIndex());
}

void MainWindow::createTileLibDockWindow(TileLib* a_tileLib, DungeonMapTile::TileType a_tileType)
{
    if (a_tileLib->includesTileType(a_tileType))
    {
        QListWidget* tiles = new QListWidget();
        a_tileLib->collection()->getStackedWidget(a_tileType)->addWidget(tiles);
        a_tileLib->setListWidget(a_tileType, tiles);

        foreach (DungeonMapTile* tile, a_tileLib->tilesById() )
        {
            createTileLibDockWindow(a_tileLib, a_tileType, tile);
        }

        a_tileLib->collection()->getPageWidget(a_tileType)->addItem(a_tileLib->name());

        connect( tiles, SIGNAL(currentItemChanged( QListWidgetItem *, QListWidgetItem *)),
                 this, SLOT(tileSelectionChanged( QListWidgetItem *, QListWidgetItem *)) );

        connect(this, SIGNAL(clearTileLibSelection()), tiles, SLOT(clearSelection()));
    }
}

void MainWindow::createTileLibDockWindow(TileLib* a_tileLib, DungeonMapTile::TileType a_tileType, DungeonMapTile* a_tile)
{
    if ( a_tile->isTileType(a_tileType) )
    {
        QListWidgetItem * item = new QListWidgetItem( a_tile->name() , a_tileLib->listWidget(a_tileType) );
        item->setIcon( a_tile->icon() );
        item->setData( m_tileRole, QVariant( a_tile->id() ) );
    }
}

void MainWindow::newLib()
{
    NewTileLib dlg(m_tileLibCollection,statusBar(),this);
    dlg.setAuthor(m_author);
    dlg.setLibsToInclude(true,true,true,false);
    if (dlg.exec())
    {
        TileLib* newLib = new TileLib(m_tileLibCollection);
        newLib->setDomain(dlg.libDomain());
        newLib->setName(dlg.libName());
        m_author = dlg.author();
        newLib->setAuthor(m_author);
        newLib->setCreated(QDateTime::currentDateTime());
        newLib->setTileSize(dlg.tileSize());
        newLib->setLibsToInclude(dlg.includesGroundTiles(),dlg.includesObjectTiles(),
                                 dlg.includesNoteTiles(),dlg.includesFogOfWarTiles());
        addLib(newLib);
    }
}

void MainWindow::newTile()
{
    if (!NewTile::checkForLibs(m_tileLibCollection))
    {
        QMessageBox::information(this, tr("New tile failed"),tr("You have to create a lib before you can add tiles."));
        return;
    }

    NewTile dlg(m_tileLibCollection, statusBar(), this);
    if(dlg.exec())
    {
        TileLib* tileLib = m_tileLibCollection->getTileLib(dlg.tileLib());
        if (tileLib)
        {
            DungeonMapTile* tile = new DungeonMapTile(dlg.tileName(),QUuid::createUuid(),"",dlg.imageName(),
                                                      dlg.tileType(),dlg.tileWidth(),dlg.tileHeight());
            tileLib->addTile(tile, true);
            tileLib->setModifier(m_author);
            tileLib->setModified(QDateTime::currentDateTime());
            createTileLibDockWindow(tileLib,tile->tileType(),tile);
        }
        else
        {
            qWarning("Failed to get tileLib!! uuid = %s", dlg.tileLib().toString().toLatin1().data());
        }
    }
}

QString MainWindow::userName() const
{
    QString user;
#ifdef WIN32
    wchar_t * username = new wchar_t[0xFF];
    DWORD size = 0xFF;
    GetUserName(username, &size);
    user.fromWCharArray(username,size);
    delete [] username;
#else
    user = getlogin();
    if (user.isEmpty())
    {
        user = getenv("LOGNAME");
    }
#endif
    return user;
}

 void MainWindow::readSettings()
 {
     QSettings settings("DungeonMapper", "DungeonMapper");
     QPoint pos = settings.value("pos", QPoint(200, 200)).toPoint();
     QSize size = settings.value("size", QSize(400, 400)).toSize();
     move(pos);
     resize(size);

     m_author = settings.value("author", userName()).toString();
 }

 void MainWindow::writeSettings()
 {
     QSettings settings("DungeonMapper", "DungeonMapper");
     settings.setValue("pos", pos());
     settings.setValue("size", size());
     settings.setValue("author", m_author);
 }

 bool MainWindow::maybeSave()
 {
     if ( m_mapWidget->isModified() )
     {
         QMessageBox::StandardButton ret;
         ret = QMessageBox::warning(this, tr("Dungeon Mapper"),
                      tr("The document has been modified.\n"
                         "Do you want to save your changes?"),
                      QMessageBox::Save | QMessageBox::Discard
                      | QMessageBox::Cancel);
         if (ret == QMessageBox::Save)
             return save();
         else if (ret == QMessageBox::Cancel)
             return false;
     }
     return true;
 }

 void MainWindow::loadFile(const QString& a_fileName)
 {

     QFile file( a_fileName );
     if ( !file.open(QFile::ReadOnly | QFile::Text) )
     {
         QMessageBox::warning( this, tr("Dungeon Mapper"),
                               tr("Cannot read file %1:\n%2.")
                               .arg( a_fileName )
                               .arg( file.errorString() ) );
         return;
     }

     QApplication::setOverrideCursor(Qt::WaitCursor);

     MapReader reader( m_setup, dynamic_cast<DungeonMapScene*>(m_mapWidget->scene()) );
     
     if ( reader.read( &file ) )
     {
         setCurrentFile( a_fileName );
         statusBar()->showMessage(tr("File loaded"), 2000);
     }

     QApplication::restoreOverrideCursor();
 }

 bool MainWindow::saveFile(const QString& a_fileName)
 {
     QFile file( a_fileName );
     if ( !file.open(QFile::WriteOnly | QFile::Text) )
     {
         QMessageBox::warning( this, tr("Dungeon Mapper"),
                               tr("Cannot write file %1:\n%2.")
                               .arg( a_fileName )
                               .arg( file.errorString() ) );
         return false;
     }

     QApplication::setOverrideCursor(Qt::WaitCursor);

     MapWriter writer( m_setup, dynamic_cast<DungeonMapScene*>(m_mapWidget->scene()) );
     if ( writer.writeFile( &file ) )
     {
         setCurrentFile( a_fileName );
         statusBar()->showMessage(tr("File saved"), 2000);
     }
     
     QApplication::restoreOverrideCursor();

     return true;
 }

 void MainWindow::setCurrentFile(const QString& a_fileName)
 {
     static int sequenceNumber = 1;

     isUntitled = a_fileName.isEmpty();
     if (isUntitled)
     {
         curFile = tr("dungeonmap%1.dmap").arg(sequenceNumber++);
     } else
     {
         curFile = QFileInfo( a_fileName ).canonicalFilePath();
     }

     m_mapWidget->setModified(false);
     setWindowModified(false);

     setWindowTitle(tr("%1[*] - %2").arg(strippedName(curFile))
                                        .arg(tr("Dungeon Mapper")));

     if (!isUntitled)
     {
        QSettings settings("DungeonMapper", "DungeonMapper");
        QStringList files = settings.value("recentFileList").toStringList();
        files.removeAll(a_fileName);
        files.prepend(a_fileName);
        while (files.size() > MaxRecentFiles)
           files.removeLast();

        settings.setValue("recentFileList", files);

        foreach (QWidget *widget, QApplication::topLevelWidgets()) {
           MainWindow *mainWin = qobject_cast<MainWindow *>(widget);
           if (mainWin)
              mainWin->updateRecentFileActions();
        }
     }
 }

 void MainWindow::updateRecentFileActions()
 {
     QSettings settings("DungeonMapper", "DungeonMapper");
     QStringList files = settings.value("recentFileList").toStringList();

     int numRecentFiles = qMin(files.size(), (int)MaxRecentFiles);

     for (int i = 0; i < numRecentFiles; ++i) {
         QString text = tr("&%1 %2").arg(i + 1).arg(strippedName(files[i]));
         recentFileActs[i]->setText(text);
         recentFileActs[i]->setData(files[i]);
         recentFileActs[i]->setVisible(true);
     }
     for (int j = numRecentFiles; j < MaxRecentFiles; ++j)
         recentFileActs[j]->setVisible(false);

     separatorAct->setVisible(numRecentFiles > 0);
 }

 QString MainWindow::strippedName(const QString& a_fullFileName)
 {
     return QFileInfo( a_fullFileName ).fileName();
 }

 MainWindow *MainWindow::findMainWindow(const QString& a_fileName)
 {
     QString canonicalFilePath = QFileInfo( a_fileName ).canonicalFilePath();

     foreach ( QWidget *widget, qApp->topLevelWidgets() )
     {
         MainWindow *mainWin = qobject_cast<MainWindow *>(widget);
         if ( mainWin && mainWin->curFile == canonicalFilePath )
         {
             return mainWin;
         }
     }
     return 0;
 }
