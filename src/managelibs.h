////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2013 Anders Reggestad
//
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#ifndef MANAGELIBS_H
#define MANAGELIBS_H

#include <QDialog>
#include <QStatusBar>
#include <QTreeWidgetItem>
#include <QNetworkAccessManager>

#include "tilelibcollection.h"
#include "dungeonmapsetup.h"

namespace Ui {
class ManageLibs;
}

class ManageLibs : public QDialog
{
    Q_OBJECT
    
public:
    explicit ManageLibs(QNetworkAccessManager* a_networkAccessManager, TileLibCollection* a_tileLibCollection, QStatusBar* a_statusBar, DungeonMapSetup* a_setup, QWidget *parent = 0);
    ~ManageLibs();

private slots:
    void tabChanged(int index);
    void download();
    void downloadItemChanged(QTreeWidgetItem* current,QTreeWidgetItem* previous);
    void update();
    void updateItemChanged(QTreeWidgetItem* current,QTreeWidgetItem* previous);
    void upload();
    void uploadItemChanged(QTreeWidgetItem* current,QTreeWidgetItem* previous);

    void updateDataReadProgress(qint64 bytesRead, qint64 totalBytes);
    void replyFinished(QNetworkReply * a_reply);
    
private:

    void uploadReplyFinished(QNetworkReply * a_reply);
    void listReplyFinished(QNetworkReply * a_reply);
    void downloadReplyFinished(QNetworkReply * a_reply);

    void upload(TileLib* lib);
    void requestList();

    Ui::ManageLibs *ui;

    QNetworkAccessManager* m_networkAccessManager;
    TileLibCollection *m_tileLibCollection;
    DungeonMapSetup *m_setup;
    QStatusBar* m_statusBar;

    TileLib* m_uploadTileLib;
    QUuid m_downloadLibUuid; // The UUID of the selected lib for downloading
    int m_downloadLibId;

    QNetworkReply* m_downloadReply;
    QNetworkReply* m_listReply;
    QNetworkReply* m_uploadReply;
};

#endif // MANAGELIBS_H
