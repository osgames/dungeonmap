////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2009 Anders Reggestad
// 
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#include "mapreader.h"


MapReader::MapReader( DungeonMapSetup* a_setup, DungeonMapScene* a_scene )
{
    m_setup = a_setup;
    m_scene = a_scene;
}

bool MapReader::read( QIODevice* a_device )
{
    setDevice( a_device );

    while (!atEnd())
    {
        readNext();

        if (isStartElement())
        {
            if (name() == "dmap" && (attributes().value("version") == "1.0" ||
                                     attributes().value("version") == "1.1" ))
            {
                readDMap();
            }
            else
            {
                raiseError(QObject::tr("The file is not an DMAP version 1.0/1.1 file."));
            }
        }
    }

    return !error();
}

void MapReader::readDMap()
{
    Q_ASSERT(isStartElement() && name() == "dmap" );

    while ( !atEnd() )
    {
        readNext();

        if ( isEndElement() )
            break;

        if ( isStartElement() )
        {
            if (name() == "tiles")
            {
                readTiles();
            }
            else if (name() == "level")
            {
                readLevel();
            }
            else
            {
                readUnknownElement();
            }
         }
     }
   
}

void MapReader::readTiles()
{
    Q_ASSERT( isStartElement() && name() == "tiles" );
    
    while ( !atEnd() )
    {
        readNext();

        if ( isEndElement() )
            break;

        if ( isStartElement() )
        {
            if ( name() == "tile" )
            {
                readTile();
            }
            else
            {
                readUnknownElement();
            }
            
        }
    }
}

void MapReader::readTile()
{
    Q_ASSERT( isStartElement() && name() == "tile" );
    
    int id = attributes().value("id").toString().toInt();

    DungeonMapTile* tile = NULL;
    QString value;
    if (attributes().hasAttribute("uuid")) // From version 1.1.
    {
        value = attributes().value("uuid").toString();
        QUuid uuid = QUuid(value);
        tile = m_setup->tileFactory()->getTile( uuid );
    }
    else if (attributes().hasAttribute("name")) // Version 1.0
    {
        value = attributes().value("name").toString();
        tile = m_setup->tileFactory()->getTile( value ); // uniqueId
    }

    if ( tile )
    {
        m_tiles[ id ] = tile;
    }
    else
    {
        raiseError( QString("Can't find the definition for the tile: ").append( value ) );
    }

    // Read anything until next end element
    readUnknownElement();
}


void MapReader::readLevel()
{
    Q_ASSERT( isStartElement() && name() == "level" );

    int width = attributes().value("width").toString().toInt();
    int height = attributes().value("height").toString().toInt();
    DungeonMapSetup::TileSize tileSize = DungeonMapSetup::TILE_SIZE_72_DPI;

    if (attributes().hasAttribute("tile_size"))
    {
        tileSize = (DungeonMapSetup::TileSize)attributes().value("tile_size").toString().toInt();
    }

    m_setup->setWidth( width );
    m_setup->setHeight( height );
    m_setup->setTileSize( tileSize );

    m_scene->setSceneRect( m_scene->itemsBoundingRect() );
    
    
    while ( !atEnd() )
    {
        readNext();

        if ( isEndElement() )
            break;

        if ( isStartElement() )
        {
            if (name() == "maptiles")
            {
                readMapTiles();
            }
            else
            {
                readUnknownElement();
            }
        }
    }
}

void MapReader::readMapTiles()
{
    Q_ASSERT( isStartElement() && name() == "maptiles" );
    
    while ( !atEnd() )
    {
        readNext();

        if ( isEndElement() )
            break;

        if ( isStartElement() )
        {
            if (name() == "tile")
            {
                readMapTile();
            }
            else
            {
                readUnknownElement();
            }
        }
    }
}

void MapReader::readMapTile()
{
    Q_ASSERT( isStartElement() && name() == "tile" );

    int id = attributes().value("id").toString().toInt();
    int posX = attributes().value("x").toString().toInt();
    int posY = attributes().value("y").toString().toInt();
    int rotate = attributes().value("r").toString().toInt();
    qreal zValue = attributes().value("z").toString().toFloat();
    

    DungeonMapTile* tile = m_tiles[ id ];
    if ( tile == NULL )
    {
        raiseError( QString( "No tile definiton with id: " ).append( QString().setNum(id) ) );
        return;
    }
    

    TileItem* item = new TileItem( tile, posX, posY, m_setup->tileSize(), rotate );
    item->setZValue( zValue );
    m_scene->addItem( item );

    // Read anything until next end element
    readUnknownElement();
}

void MapReader::readUnknownElement()
{
    Q_ASSERT( isStartElement() );
    
    while ( !atEnd() )
    {
        readNext();

        if ( isEndElement() )
            break;

        if ( isStartElement() )
        {
            readUnknownElement();
        }
     }
 }
