////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2009 Anders Reggestad
// 
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#ifndef DUNGEONMAPTILE_H_
#define DUNGEONMAPTILE_H_

#include <QString>
#include <QIcon>
#include <QImage>
#include <QUuid>

class TileLib;

class DungeonMapTile
{
 public:
    enum TileType {
        UNKNOWN_TILE,
        GROUND_TILE,
        OBJECT_TILE,
        NOTE_TILE,
        FOG_OF_WAR_TILE
    };
    static QString m_tileTypeNames[];


    DungeonMapTile( int a_id, const QString& a_name, const QString& a_uniqueId, const QString& a_imageName, TileType a_tileType, int a_width, int a_height );
    DungeonMapTile( const QString& a_name, const QUuid& a_uuid, const QString& a_uniqueId, const QString& a_imageName, TileType a_tileType, int a_width, int a_height );
    DungeonMapTile( const QString& a_name, const QUuid& a_uuid, const QString& a_imageName, TileType a_tileType, int a_width, int a_height );
    DungeonMapTile( const QString& a_name, const QUuid& a_uuid, const QString& a_uniqueId, const QImage& a_image, TileType a_tileType, int a_width, int a_height );

    int id() const;
    bool isGroundTile() const;
    bool isObjectTile() const;
    bool isNoteTile() const;
    bool isFogOfWarTile() const;
    bool isTileType(DungeonMapTile::TileType a_tileType) const;
    const QString& typeName() const;
    const QString& name() const;
    const QString& uniqueId() const;
    const QUuid& uuid() const;
    const QIcon& icon() const;
    const QString& imageName() const;
    const QImage& image() const;
    DungeonMapTile::TileType tileType() const;
    int width() const;
    int height() const;

    void setId(int id);
    void setLib(TileLib* a_tileLib);
    TileLib* tileLib();

    static TileType stringToTileType(const QString& typeName);

 private:
    int      m_id;
    TileLib* m_tileLib;
    QString  m_name;
    QUuid    m_uuid;
    QString  m_uniqueId; // Deprecated, but needed to read old map files
    QIcon    m_icon;
    QString  m_imageName;
    QImage   m_image;
    TileType m_tileType;
    int      m_width;
    int      m_height;
    
};

class DungeonMapTileFactoryIf
{
 public:
    virtual DungeonMapTile* getTile( int a_id ) = 0;
    virtual DungeonMapTile* getTile( const QString& a_uniqueId ) = 0;
    virtual DungeonMapTile* getTile( const QUuid& a_uuid ) = 0;
};    

#endif
