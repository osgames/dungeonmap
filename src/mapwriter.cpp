////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2009 Anders Reggestad
// 
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#include "mapwriter.h"

    

MapWriter::MapWriter( DungeonMapSetup* a_setup, DungeonMapScene* a_scene )
{
    m_setup = a_setup;
    m_scene = a_scene;
    setAutoFormatting(true);
}

bool MapWriter::writeFile( QIODevice* a_device )
{
    if (m_scene == NULL || m_setup == NULL) return false;

    setDevice( a_device );
     
    writeStartDocument();
    writeDTD("<!DOCTYPE dmap>");
    writeStartElement("dmap");
    writeAttribute("version", "1.1");

    writeTiles();
    writeLevel();

    writeEndDocument();
    return true;
    
}

void MapWriter::writeTiles()
{
    writeStartElement("tiles");
    
    QList<QGraphicsItem *> itemList = m_scene->items ();

    // Find the set of tiles used in this map
    QMap< int, const DungeonMapTile* > uniqueTiles;
    foreach( QGraphicsItem* item, itemList )
    {
        TileItem* tileItem = dynamic_cast<TileItem*>(item);
        if (tileItem && !uniqueTiles.contains( tileItem->tile()->id()) )
        {
            uniqueTiles[ tileItem->tile()->id() ] = tileItem->tile();
        }
    }
    
    // Write the tiles
    foreach( const DungeonMapTile* tile, uniqueTiles )
    {
        QString value;
        writeEmptyElement("tile");
        writeAttribute("uuid", tile->uuid().toString());
        value.setNum( tile->id() );
        writeAttribute("id", value );
    }

    writeEndElement();
    
}


void MapWriter::writeLevel()
{
    writeStartElement("level");

    QString widthStr;
    widthStr.setNum( m_setup->width() );
    QString heightStr;
    heightStr.setNum( m_setup->height() );
    QString tileSizeStr;
    tileSizeStr.setNum( m_setup->tileSize() );
    writeAttribute("width", widthStr );
    writeAttribute("height", heightStr );
    writeAttribute("tile_size", heightStr );


    writeStartElement("maptiles");

    QList<QGraphicsItem *> itemList = m_scene->items ();
    foreach( QGraphicsItem* item, itemList )
    {
        TileItem* tileItem = dynamic_cast<TileItem*>(item);
        if (tileItem)
        {
            writeItem(tileItem);
        }
    }
    
    writeEndElement();
}



void MapWriter::writeItem(TileItem *item)
{
    QString value;

    writeEmptyElement("tile");
    
    value.setNum( item->tile()->id() );
    writeAttribute( "id", value );

    value.setNum( item->posX() );
    writeAttribute( "x", value );

    value.setNum( item->posY() );
    writeAttribute( "y", value );

    value.setNum( item->zValue() );
    writeAttribute( "z", value );

    value.setNum( item->rotate() );
    writeAttribute( "r", value );

}

