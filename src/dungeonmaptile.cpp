////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2009 Anders Reggestad
// 
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#include "dungeonmaptile.h"

QString DungeonMapTile::m_tileTypeNames[] = {QString("unknown"),
                                             QString("ground"),
                                             QString("object"),
                                             QString("note"),
                                             QString("fog_of_war"),
                                             QString()};

DungeonMapTile::DungeonMapTile( int a_id, const QString& a_name, const QString& a_uniqueId,
                                const QString& a_imageName, TileType a_tileType, int a_width, int a_height )
{
    m_id = a_id;
    m_tileLib = NULL;
    m_name = a_name;
    m_uniqueId = a_uniqueId;
    m_uuid = QUuid::createUuid(); // Temp, until interface is changed and standard is saved...
    m_icon = QIcon( a_imageName );
    m_imageName = a_imageName;
    m_image = QImage( a_imageName );
    m_tileType = a_tileType;
    m_width = a_width;
    m_height = a_height;
}

DungeonMapTile::DungeonMapTile( const QString& a_name, const QUuid& a_uuid, const QString& a_uniqueId,
                                const QString& a_imageName, TileType a_tileType,
                                int a_width, int a_height ):
    m_id(-1),
    m_tileLib(NULL),
    m_name(a_name),
    m_uuid(a_uuid),
    m_uniqueId(a_uniqueId),
    m_imageName(a_imageName),
    m_tileType(a_tileType),
    m_width(a_width),
    m_height(a_height)
{
    m_icon = QIcon( a_imageName );
    m_image = QImage( a_imageName );
}

DungeonMapTile::DungeonMapTile( const QString& a_name, const QUuid& a_uuid,
                                const QString& a_imageName, TileType a_tileType,
                                int a_width, int a_height ):
    m_id(-1),
    m_tileLib(NULL),
    m_name(a_name),
    m_uuid(a_uuid),
    m_imageName(a_imageName),
    m_tileType(a_tileType),
    m_width(a_width),
    m_height(a_height)
{
    m_icon = QIcon( a_imageName );
    m_image = QImage( a_imageName );
}

DungeonMapTile::DungeonMapTile( const QString& a_name, const QUuid& a_uuid,
                                const QString& a_uniqueId, const QImage& a_image,
                                TileType a_tileType,
                                int a_width, int a_height ):
    m_name(a_name),
    m_uuid(a_uuid),
    m_uniqueId(a_uniqueId),
    m_tileType(a_tileType),
    m_width(a_width),
    m_height(a_height)
{
    m_icon = QIcon(QPixmap::fromImage(a_image));
    m_image = a_image;
}

int DungeonMapTile::id() const
{
    return m_id;
}

bool DungeonMapTile::isGroundTile() const
{
    return m_tileType == GROUND_TILE;
}

bool DungeonMapTile::isObjectTile() const
{
    return m_tileType == OBJECT_TILE;
}

bool DungeonMapTile::isNoteTile() const
{
    return m_tileType == NOTE_TILE;
}

bool DungeonMapTile::isFogOfWarTile() const
{
    return m_tileType == FOG_OF_WAR_TILE;
}

bool DungeonMapTile::isTileType(DungeonMapTile::TileType a_tileType) const
{
    return m_tileType == a_tileType;
}

const QString& DungeonMapTile::typeName() const
{
    return m_tileTypeNames[m_tileType];
}

const QString& DungeonMapTile::name() const
{
    return m_name;
}

const QUuid& DungeonMapTile::uuid() const
{
    return m_uuid;
}

const QString& DungeonMapTile::uniqueId() const
{
    return m_uniqueId;
}

const QIcon& DungeonMapTile::icon() const
{
    return m_icon;
}

const QString& DungeonMapTile::imageName() const
{
    return m_imageName;
}

const QImage& DungeonMapTile::image() const
{
    return m_image;
}

DungeonMapTile::TileType DungeonMapTile::tileType() const
{
    return m_tileType;
}


int DungeonMapTile::width() const
{
    return m_width;
}

int DungeonMapTile::height() const
{
    return m_height;
}

void DungeonMapTile::setId(int id)
{
    m_id = id;
}

void DungeonMapTile::setLib(TileLib *a_tileLib)
{
    m_tileLib = a_tileLib;
}

TileLib* DungeonMapTile::tileLib()
{
    return m_tileLib;
}

DungeonMapTile::TileType DungeonMapTile::stringToTileType(const QString& typeName)
{
   int index = 0;
   while (!m_tileTypeNames[index].isEmpty())
   {
       if (m_tileTypeNames[index].compare(typeName,Qt::CaseInsensitive) == 0)
       {
           return (TileType)index;
       }
       index++;
   }
   return UNKNOWN_TILE;
}



