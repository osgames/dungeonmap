////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2009 Anders Reggestad
// 
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#ifndef DUNGEONMAPSCENE_H_
#define DUNGEONMAPSCENE_H_

#include <QGraphicsScene>
#include <QUndoStack>

#include "dungeonmapsetup.h"

class TileItem;

class DungeonMapScene : public QGraphicsScene
{
    Q_OBJECT
        
 public:
    DungeonMapScene( QWidget* parent, DungeonMapSetup* setup );
    virtual ~DungeonMapScene();

    virtual void mousePressEvent ( QGraphicsSceneMouseEvent * mouseEvent );
    virtual void mouseMoveEvent ( QGraphicsSceneMouseEvent * mouseEvent );
    virtual void mouseReleaseEvent ( QGraphicsSceneMouseEvent * mouseEvent );

    QUndoStack* undoStack() const;

    void toggleNotes(bool checked);
    bool viewNotes() const;

    virtual void addTileItem(TileItem *item);
    virtual void removeTileItem(TileItem *item);

 signals:
    void contentsChanged();

 private:

    virtual void handleMouseDownAt( const QPoint& point );

    virtual void doFill( const QPoint& point, int lastId );

    TileItem* findGroundTileAt( const QPoint& point );
    
    DungeonMapSetup* m_setup;
    bool             m_isPainting;
    QPoint           m_lastPoint;
    QWidget*         m_parent;
    QUndoStack*      m_undoStack;
    bool             m_viewNotes;
    QList<TileItem*> m_notes;
};

#endif
