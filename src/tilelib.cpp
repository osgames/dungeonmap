////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2013 Anders Reggestad
//
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#include "tilelib.h"
#include "tilelibcollection.h"

TileLib::TileLib(TileLibCollection* a_tileLibCollection) :
    m_uuid(QUuid::createUuid()),
    m_created(QDateTime::currentDateTime()),
    m_modified(QDateTime::currentDateTime()),
    m_tileSize(72),
    m_includesGroundTiles(true),
    m_includesObjectTiles(true),
    m_includesNoteTiles(true),
    m_includesFogOfWarTiles(true),
    m_tileLibCollection(a_tileLibCollection),
    m_dirty(false),
    m_shared(false)
{
}

TileLib::TileLib(TileLibCollection* a_tileLibCollection, const QString& a_domain, const QString& a_name) :
    m_uuid(QUuid::createUuid()),
    m_domain(a_domain),
    m_name(a_name),
    m_created(QDateTime::currentDateTime()),
    m_modified(QDateTime::currentDateTime()),
    m_tileSize(72),
    m_includesGroundTiles(true),
    m_includesObjectTiles(true),
    m_includesNoteTiles(true),
    m_includesFogOfWarTiles(true),
    m_tileLibCollection(a_tileLibCollection),
    m_dirty(false),
    m_shared(false)
{
}

TileLib::~TileLib()
{
    qDeleteAll(m_tilesById);
}

const QUuid& TileLib::uuid() const
{
    return m_uuid;
}

const QString& TileLib::domain() const
{
    return m_domain;
}

const QString& TileLib::name() const
{
    return m_name;
}

const QDateTime& TileLib::created() const
{
    return m_created;
}

const QString& TileLib::author() const
{
    return m_author;
}

const QDateTime& TileLib::modified() const
{
    return m_modified;
}

const QString& TileLib::modifier() const
{
    return m_modifier;
}

int TileLib::tileSize() const
{
    return m_tileSize;
}

bool TileLib::includesGroundTiles() const
{
    return m_includesGroundTiles;
}

bool TileLib::includesObjectTiles() const
{
    return m_includesObjectTiles;
}

bool TileLib::includesNoteTiles() const
{
    return m_includesNoteTiles;
}

bool TileLib::includesFogOfWarTiles() const
{
    return m_includesFogOfWarTiles;
}

bool TileLib::includesTileType( DungeonMapTile::TileType a_tileType ) const
{
    switch (a_tileType)
    {
    case DungeonMapTile::UNKNOWN_TILE:
        return false;
    case DungeonMapTile::GROUND_TILE:
        return includesGroundTiles();
    case DungeonMapTile::OBJECT_TILE:
        return includesObjectTiles();
    case DungeonMapTile::NOTE_TILE:
        return includesNoteTiles();
    case DungeonMapTile::FOG_OF_WAR_TILE:
        return includesFogOfWarTiles();
    }
    return false;
}

TileLibCollection* TileLib::collection()
{
    return m_tileLibCollection;
}

QListWidget* TileLib::listWidget(DungeonMapTile::TileType a_tileType)
{
    return m_listWidgets[a_tileType];
}

QMap<int, DungeonMapTile*>& TileLib::tilesById()
{
    return m_tilesById;
}

DungeonMapTile* TileLib::getTile( int a_id )
{
    return m_tilesById[ a_id ];
}

DungeonMapTile* TileLib::getTile( const QString& a_uniqueId )
{
    return m_tilesByUniqueId[ a_uniqueId ];
}

DungeonMapTile* TileLib::getTile( const QUuid& a_uuid )
{
    return m_tilesByUuid[ a_uuid ];
}

void TileLib::setUuid(const QUuid& a_uuid)
{
    m_uuid = a_uuid;
}

void TileLib::setDomain(const QString& domain)
{
    m_domain = domain;
}

void TileLib::setName(const QString& name)
{
    m_name = name;
}

void TileLib::setCreated(const QDateTime& created)
{
    m_created = created;
}

void TileLib::setAuthor(const QString& author)
{
    m_author = author;
}

void TileLib::setModified(const QDateTime& modified)
{
    m_modified = modified;
}

void TileLib::setModifier(const QString& modifier)
{
    m_modifier = modifier;
}

void TileLib::setTileSize(int a_tileSize)
{
    m_tileSize = a_tileSize;
}

void TileLib::setLibsToInclude(bool groundTiles, bool objectTiles, bool noteTiles, bool fogOfWarTiles)
{
    m_includesGroundTiles = groundTiles;
    m_includesObjectTiles = objectTiles;
    m_includesNoteTiles = noteTiles;
    m_includesFogOfWarTiles = fogOfWarTiles;
}

void TileLib::setListWidget(DungeonMapTile::TileType a_tileType, QListWidget *a_widget)
{
    m_listWidgets[ a_tileType ] = a_widget;
}

void TileLib::addTile(DungeonMapTile *a_tile, bool updateDirtyFlag)
{
    a_tile->setId(m_tileLibCollection->getNextTileId());
    a_tile->setLib(this);

    m_tilesById[a_tile->id()] = a_tile;
    m_tilesByUuid[a_tile->uuid()] = a_tile;
    m_tilesByUniqueId[a_tile->uniqueId()] = a_tile;

    m_tileLibCollection->addTile(a_tile);

    if (updateDirtyFlag)
    {
       m_dirty = true;
    }
}

bool TileLib::isDirty() const
{
    return m_dirty;
}

void TileLib::setDirty(bool dirty)
{
    m_dirty = dirty;
}

bool TileLib::isShared() const
{
    return m_shared;
}

void TileLib::setShared(bool shared)
{
    m_shared = shared;
}
