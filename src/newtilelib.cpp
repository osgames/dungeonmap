////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2013 Anders Reggestad
//
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#include "newtilelib.h"
#include "ui_newtilelib.h"
#include "dungeonmapsetup.h"

NewTileLib::NewTileLib(TileLibCollection *a_tileLibCollection, QStatusBar* a_statusBar, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewTileLib),
    m_tileLibCollection(a_tileLibCollection),
    m_statusBar(a_statusBar)
{
    ui->setupUi(this);

    ui->m_domain->addItem(tr("Dungeon Mapper"));
    ui->m_domain->addItem(tr("General Fantacy"));
    ui->m_domain->setCurrentText(tr("General Fantacy"));

    ui->m_tileSize->addItem("72 dpi",QVariant(DungeonMapSetup::TILE_SIZE_72_DPI));
    ui->m_tileSize->addItem("96 dpi",QVariant(DungeonMapSetup::TILE_SIZE_96_DPI));
    ui->m_tileSize->addItem("100 dpi",QVariant(DungeonMapSetup::TILE_SIZE_100_DPI));
    ui->m_tileSize->addItem("150 dpi",QVariant(DungeonMapSetup::TILE_SIZE_150_DPI));
    ui->m_tileSize->addItem("300 dpi",QVariant(DungeonMapSetup::TILE_SIZE_300_DPI));
    ui->m_tileSize->addItem("600 dpi",QVariant(DungeonMapSetup::TILE_SIZE_600_DPI));
    ui->m_tileSize->addItem("1200 dpi",QVariant(DungeonMapSetup::TILE_SIZE_1200_DPI));

    ui->m_name->setFocus();
}

NewTileLib::~NewTileLib()
{
    delete ui;
}

void NewTileLib::setLibsToInclude(bool groundTiles, bool objectTiles, bool noteTiles, bool fogOfWarTiles)
{
    ui->m_includeGroundTiles->setChecked(groundTiles);
    ui->m_includeObjectTiles->setChecked(objectTiles);
    ui->m_includeNoteTiles->setChecked(noteTiles);
    ui->m_includeFogOfWarTiles->setChecked(fogOfWarTiles);
}

void NewTileLib::setAuthor(const QString &author)
{
    ui->m_author->setText(author);
}

const QString NewTileLib::libDomain() const
{
    return ui->m_domain->currentText();
}

const QString NewTileLib::libName() const
{
    return ui->m_name->text();
}

const QString NewTileLib::author() const
{
    return ui->m_author->text();
}

bool NewTileLib::includesGroundTiles() const
{
    return ui->m_includeGroundTiles->isChecked();
}

bool NewTileLib::includesObjectTiles() const
{
    return ui->m_includeObjectTiles->isChecked();
}

bool NewTileLib::includesNoteTiles() const
{
    return ui->m_includeNoteTiles->isChecked();
}

bool NewTileLib::includesFogOfWarTiles() const
{
    return ui->m_includeFogOfWarTiles->isChecked();
}

int NewTileLib::tileSize() const
{
    return ui->m_tileSize->itemData(ui->m_tileSize->currentIndex()).toInt();
}

void NewTileLib::done(int r)
{
    if(QDialog::Accepted == r)  // ok was pressed
    {
        // validate the data somehow
        if (ui->m_domain->currentText().isEmpty())
        {
            m_statusBar->showMessage(tr("Domain is missing."));
            return;
        }

        if (ui->m_name->text().isEmpty())
        {
            m_statusBar->showMessage(tr("Name is missing."));
            return;
        }

        if (ui->m_author->text().isEmpty())
        {
            m_statusBar->showMessage(tr("Author is missing."));
            return;
        }

        TileLib* tileLib = m_tileLibCollection->getTileLib(ui->m_name->text());
        if (tileLib)
        {
            m_statusBar->showMessage(tr("Tile lib name is not uniq."));
            return;
        }

        QDialog::done(r);
        return;
    }
    else    // cancel, close or exc was pressed
    {
        QDialog::done(r);
        return;
    }
}
