////////////////////////////////////////////////////////////////////////////////
// DungeonMapper help you draw your dungeons for your next adventure.
//
// Copyright (C) 2009 Anders Reggestad
// 
// This file is part of DungeonMapper.
//
// DungeonMapper is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// DungeonMapper is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with DungeonMapper.  If not, see <http://www.gnu.org/licenses/>.
//
// E-mail: andersr@pvv.org
// Mail:   Anders Reggestad, Tregata 54, 3330 Skotselv, Norway
//
////////////////////////////////////////////////////////////////////////////////
#ifndef MAPWIDGET_H
#define MAPWIDGET_H

#include <QGraphicsView>

#include "dungeonmapsetup.h"
#include "griditem.h"
#include "compassitem.h"

class DungeonMapScene;


class MapWidget : public QGraphicsView
{
    Q_OBJECT

 public:
    MapWidget( DungeonMapSetup* setup );
    virtual ~MapWidget();


    void itemMoved();
    
    bool isModified();
    void setModified( bool modified );

    bool isEmpty();
    
    DungeonMapScene* scene();

 public slots:
    void toggleCompass(bool checked);
    void toggleNotes(bool checked);

 protected:
    void keyPressEvent(QKeyEvent *event);
    void wheelEvent(QWheelEvent *event);

    void drawBackground(QPainter *painter, const QRectF &rect);

 private:
    void scaleView(qreal scaleFactor);

    bool m_modified;
    bool m_empty;

    DungeonMapScene* m_scene;
    CompassItem* m_compassItem;
    GridItem* m_gridItem;

};

#endif
